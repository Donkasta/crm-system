package kg.attractor.esdp.mercsgroup.dtos.authorization;

import kg.attractor.esdp.mercsgroup.validators.UniqueINN;
import kg.attractor.esdp.mercsgroup.validators.UniqueLogin;
import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Validated
@Builder
public class ManagerRegistrationDTO {

    @NotBlank(message = "Введите ваш email")
    @Email
    private String email;

    @NotBlank(message = "Введите логин")
    @NotNull
    @UniqueLogin
    private String login;
    
    @NotBlank(message = "Введите пароль")
    @Size(min = 8, max = 16, message = "Пароль должен содержать минимум 8 символов")
    private String password;

    @NotBlank(message = "Введите ваше имя")
    private String fullName;

    @UniquePhone
    @NotBlank(message = "Введите номер телефона")
    @Size(min = 12, message = "Введите номер телефон пример: +996000000000")
    private String phone;

    @UniqueINN
    @NotBlank(message = "Введите инн")
    @Size(min = 14, max = 14, message = "Инн не состоит из 14 цифр")
    private String inn;
}
