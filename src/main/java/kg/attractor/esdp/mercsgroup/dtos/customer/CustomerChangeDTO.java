package kg.attractor.esdp.mercsgroup.dtos.customer;

import kg.attractor.esdp.mercsgroup.validators.UniqueEmail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class CustomerChangeDTO {
    private Long id;
    @Email(message = "Введите ваш email")
    @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$", message = "Некорректный адрес электронной почты")
    @UniqueEmail(message = "Пользователь с таким email уже существует")
    private String email;
    private String name;
    private String company;
    private String post;

}
