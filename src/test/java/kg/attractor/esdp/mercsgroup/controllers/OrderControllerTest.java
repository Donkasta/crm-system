package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderEditDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import kg.attractor.esdp.mercsgroup.services.OrderService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private OrderController orderController;

    private static final String EMAIL = "manager1@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testChangeOrderDetails_RedirectsToHomePage_WhenEditOrderSuccessful() throws Exception {

        DetailDTO detailDTO = DetailDTO.builder()
                .id(1L)
                .order(1L)
                .productUrl("https://chineseFactory.com/items/78")
                .quantity(1.0)
                .quantityUnit("шт")
                .productPrice(200.0)
                .freight(50.0)
                .customsValue(220.0)
                .build();
        List<DetailDTO> detailDTOList = new ArrayList<>();
        detailDTOList.add(detailDTO);
        OrderEditDTO orderEditDTO = OrderEditDTO.builder()
                .id(1L)
                .details(detailDTOList)
                .build();

        when(orderService.editOrder(orderEditDTO)).thenReturn(true);
        mockMvc.perform(post("/orders/change")
                        .param("details[0].id", detailDTO.getId().toString())
                        .param("details[0].order", detailDTO.getOrder().toString())
                        .param("details[0].quantityUnit", detailDTO.getQuantityUnit())
                        .param("details[0].productUrl", detailDTO.getProductUrl())
                        .param("details[0].quantity", detailDTO.getQuantity().toString())
                        .param("details[0].productPrice", detailDTO.getProductPrice().toString())
                        .param("details[0].freight", detailDTO.getFreight().toString())
                        .param("details[0].customsValue", detailDTO.getCustomsValue().toString())
                        .param("id", orderEditDTO.getId().toString())
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(orderService, times(1)).editOrder(orderEditDTO);
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testChangeOrderDetails_RedirectsToHomePage_WhenEditOrderUnsuccessful() throws Exception {
        mockMvc.perform(post("/orders/change")
                        .with(csrf()))
                .andExpect(status().is4xxClientError());
        verify(orderService, times(1)).editOrder(any());
    }

    @Test
    void getOrderByIdAndPopulateModel() {
        Long orderId = 1L;
        Order order = TestUtil.createOrder(EMAIL);
        Employee employee = TestUtil.createEmployee();

        when(orderService.getManagerOrder(orderId, employee.getId())).thenReturn(order);

        Model model = mock(Model.class);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                EMAIL,
                PASSWORD,
                Collections.singletonList(new SimpleGrantedAuthority("Manager"))
        );

        when(employeeService.findEmployee(authentication.getName())).thenReturn(employee);

        String viewName = orderController.getOrder(orderId, model,authentication);

        assertEquals("order", viewName);
        verify(orderService, times(1)).getManagerOrder(orderId, employee.getId());
        verify(employeeService, times(1)).findEmployee(authentication.getName());
        verify(model, times(1)).addAttribute(eq("order"), eq(OrderDTO.from(order)));
    }

    @Test
    public void testConfirmOrder_Successful() throws Exception{
        Long id = 1L;
        Order order = TestUtil.createOrder(EMAIL);
        String status = "Ожидается подтверждение";
        when(orderService.getOrderById(id)).thenReturn(order);

        mockMvc.perform(post("/orders/confirm")
                .param("orderId", "1")
                .param("status", status)
                .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders/createOffer?order_id=" + order.getId()));

        verify(orderService, times(1)).getOrderById(id);
        verify(orderService, times(1)).confirmOrder(order, status);
    }
}