package kg.attractor.esdp.mercsgroup.dtos.employee;

import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class EmployeeChangeNumberDTO {
    private Long id;
    @UniquePhone(message = "Этот номер телефона уже используется")
    @Pattern(regexp = "^(\\+996\\s?)?(\\d{3}\\s?)?\\d{6}$", message = "Введите номер телефона в формате: +996000000000")
    private String number;
}
