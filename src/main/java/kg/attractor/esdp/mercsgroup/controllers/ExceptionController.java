package kg.attractor.esdp.mercsgroup.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(NoHandlerFoundException.class)
    public String handleNotFound() {
        return "redirect:/error/404";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    private String nullPointer(IllegalArgumentException ex, Model model){
        model.addAttribute("message",ex.getMessage());
        return "/error/errorMessage";
    }

}
