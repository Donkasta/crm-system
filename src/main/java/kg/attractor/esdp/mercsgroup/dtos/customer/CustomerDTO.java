package kg.attractor.esdp.mercsgroup.dtos.customer;

import kg.attractor.esdp.mercsgroup.dtos.request.RequestDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Order;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class CustomerDTO {

    public static CustomerDTO from(Customer customer) {
        CustomerDTOBuilder builder = builder()
                .id(customer.getId())
                .name(customer.getName())
                .roleId(customer.getRole().getId())
                .customerData(customer.getCustomerData())
                .email(customer.getEmail())
                .company(customer.getCompany())
                .post(customer.getPost())
                .number(customer.getNumber())
                .ordersQuantity(customer.getOrdersQuantity())
                .requests(customer.getRequests().stream()
                        .map(RequestDTO::fromWithoutCustomer)
                        .collect(Collectors.toList())
                );
        if (customer.getEmployee() != null) {
            builder.employeeId(customer.getEmployee().getId());
        }

        return builder.build();

    }

    private Long id;
    private String password;
    private Long roleId;
    private String customerData;
    @Email(message = "Введите ваш email")
    private String email;
    private String name;
    private String company;
    private String post;
    private String number;
    private Long employeeId;
    private Integer ordersQuantity;
    private List<Order> orders;
    private List<RequestDTO> requests;
}
