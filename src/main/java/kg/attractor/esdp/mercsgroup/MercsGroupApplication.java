package kg.attractor.esdp.mercsgroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MercsGroupApplication {

    public static void main(String[] args) {
        SpringApplication.run(MercsGroupApplication.class, args);
    }

}
