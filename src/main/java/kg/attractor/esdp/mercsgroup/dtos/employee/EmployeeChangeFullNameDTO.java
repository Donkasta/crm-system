package kg.attractor.esdp.mercsgroup.dtos.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Validated
public class EmployeeChangeFullNameDTO {
    private Long id;
    @NotBlank(message = "имя не может быть пустым")
    @NotEmpty(message = "имя не может быть пустым")
    private String fullName;
}
