package kg.attractor.esdp.mercsgroup.dtos.customer;

import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Pattern;

@Validated
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerChangeNumberDTO {
    private Long id;
    @UniquePhone(message = "Этот номер уже используется")
    @Pattern(regexp = "^(\\+996\\s?)?(\\d{3}\\s?)?\\d{6}$", message = "Введите номер телефона в формате: +996000000000")
    private String number;
}
