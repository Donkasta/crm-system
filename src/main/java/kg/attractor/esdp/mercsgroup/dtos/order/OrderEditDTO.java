package kg.attractor.esdp.mercsgroup.dtos.order;

import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Data
@Builder
@Validated
public class OrderEditDTO {
    private Long id;
    @Valid
    private List<DetailDTO> details;
}
