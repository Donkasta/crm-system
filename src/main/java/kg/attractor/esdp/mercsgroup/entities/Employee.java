package kg.attractor.esdp.mercsgroup.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employees")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "full_name")
    private String fullName;

    private String password;
    private String login;
    private String email;
    private String inn;
    private String post;
    private String number;

    private boolean enabled;
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @OrderBy("email ASC")
    private List<Customer> customers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @OrderBy("orderDate ASC")
    private List<Order> orders;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @OrderBy("createdDateTime")
    private List<Csrf> csrfs;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", inn='" + inn + '\'' +
                ", post='" + post + '\'' +
                ", number='" + number + '\'' +
                ", enabled=" + enabled +
                ", role=" + role +
                '}';
    }
}
