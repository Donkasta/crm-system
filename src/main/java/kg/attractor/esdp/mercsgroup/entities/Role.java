package kg.attractor.esdp.mercsgroup.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
    @OrderBy("customerData ASC")
    private List<Customer> customers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
    @OrderBy("fullName ASC")
    private List<Employee> employees;

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }
}
