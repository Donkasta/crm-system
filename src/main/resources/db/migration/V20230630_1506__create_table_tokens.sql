create table if not exists tokens (
    id bigserial primary key,
    user_id bigint not null,
    role_id bigint not null references roles (id),
    token varchar not null
);
