package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.GuestRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.ManagerRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.*;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.OrderRepository;
import kg.attractor.esdp.mercsgroup.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class AdminService {
    private final CustomerRepository customerRepository;
    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final OrderRepository orderRepository;
    private final PasswordEncoder encoder;

    public Page<Customer> getCustomers(Integer page, Integer size, String role) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return customerRepository.findCustomersByRoleRole(role, pageable);
    }

    private Integer getPage(Integer page) {
        if (page != null && page > 0) {
            return page - 1;
        }
        return 0;
    }

    public Customer getGuest(Long id) {
        var guest = customerRepository.getCustomerById(id);
        if (guest.isEmpty()) {
            throw new IllegalArgumentException("Guest not found for ID: " + id);
        }
        if (!guest.get().getRole().getRole().equals("Guest")) {
            throw new IllegalArgumentException("Guest not found for ID: " + id);
        }
        return guest.get();
    }

    public Customer getCustomer(Long id) {
        var customer = customerRepository.getCustomerById(id);
        if (customer.isEmpty()) {
            throw new IllegalArgumentException("Customer not found for ID: " + id);
        }
        if (!customer.get().getRole().getRole().equals("Customer")) {
            throw new IllegalArgumentException("Customer not found for ID: " + id);
        }
        return customer.get();
    }

    public boolean emailIsUnique(String email) {
        boolean existsInCustomers = customerRepository.existsByEmail(email);
        boolean existsInEmployees = employeeRepository.existsByEmail(email);
        return !existsInCustomers && !existsInEmployees;
    }

    public Customer customerChange(CustomerDTO customerDTO) {
        var customer = customerRepository.getCustomerById(customerDTO.getId()).get();

        if (customerDTO.getPassword() != null && !customerDTO.getPassword().isBlank()) {
            customer.setPassword(encoder.encode(customerDTO.getPassword()));
        }
        if (!customerDTO.getName().isEmpty()) customer.setName(customerDTO.getName());
        if (!customerDTO.getEmail().isEmpty()) customer.setEmail(customerDTO.getEmail());
        if (!customerDTO.getCustomerData().isEmpty()) customer.setCustomerData(customerDTO.getCustomerData());
        if (!customerDTO.getCompany().isEmpty()) customer.setCompany(customerDTO.getCompany());
        if (!customerDTO.getPost().isEmpty()) customer.setPost(customerDTO.getPost());
        if (!customerDTO.getNumber().isEmpty()) customer.setNumber(customerDTO.getNumber());
        customerRepository.save(customer);
        return customer;
    }

    public boolean registerNewManager(ManagerRegistrationDTO data, Model model) {
        String email = data.getEmail();
        Optional<Employee> employee = employeeRepository.findByEmail(email);
        if (employee.isPresent()) {
            model.addAttribute("message", "Данный email уже зарегистрирован как Сотрудник!");
            return false;
        }
        Optional<Customer> customer = customerRepository.findByEmail(email);
        if (customer.isPresent()) {
            model.addAttribute("message", "Данный email уже зарегистрирован!");
            return false;
        }
        Role role = roleRepository.findById(1L).get();
        employeeRepository.save(Employee.builder()
                .email(data.getEmail())
                .password(encoder.encode(data.getPassword()))
                .fullName(data.getFullName())
                .inn(data.getInn())
                .number(data.getPhone())
                .login(data.getLogin())
                .post(role.getRole())
                .enabled(true)
                .role(role)
                .build());
        return true;
    }

    public boolean registerNewGuest(GuestRegistrationDTO data, Model model) {
        if (data.getEmail() != null && !data.getEmail().isBlank()) {
            String email = data.getEmail();
            Optional<Employee> employee = employeeRepository.findByEmail(email);
            if (employee.isPresent()) {
                model.addAttribute("message", "Данный email уже зарегистрирован как Сотрудник!");
                return false;
            }
            Optional<Customer> customer = customerRepository.findByEmail(email);
            if (customer.isPresent()) {
                model.addAttribute("message", "Данный email уже зарегистрирован!");
                return false;
            }
        }
        Role role = roleRepository.findById(4L).get();
        var customer = Customer.builder()
                .name(data.getName())
                .number(data.getPhone())
                .enabled(true)
                .role(role)
                .ordersQuantity(0)
                .build();
        if (data.getEmail() != null && !data.getEmail().isEmpty()) customer.setEmail(data.getEmail());
        if (data.getCompany() != null && !data.getCompany().isEmpty()) customer.setCompany(data.getCompany());

        customerRepository.save(customer);

        return true;
    }

    public List<Employee> getManagersForGuest() {
        return employeeRepository.findEmployeesByRoleRole("Manager");
    }

    public Customer guestChange(CustomerDTO customerDTO) {
        var customer = customerRepository.getCustomerById(customerDTO.getId()).get();
        if (!customerDTO.getName().isEmpty()) customer.setName(customerDTO.getName());
        if (!customerDTO.getEmail().isEmpty()) customer.setEmail(customerDTO.getEmail());
        if (!customerDTO.getCustomerData().isEmpty()) customer.setCustomerData(customerDTO.getCustomerData());
        if (!customerDTO.getCompany().isEmpty()) customer.setCompany(customerDTO.getCompany());
        if (!customerDTO.getNumber().isEmpty()) customer.setNumber(customerDTO.getNumber());
        if (customerDTO.getEmployeeId() != null) {
            if (customer.getEmployee() == null) {
                var employee = employeeRepository.findById(customerDTO.getEmployeeId()).get();
                customer.setEmployee(employee);
            } else if (!customerDTO.getEmployeeId().equals(customer.getEmployee().getId())) {
                var employee = employeeRepository.findById(customerDTO.getEmployeeId()).get();
                customer.setEmployee(employee);
            }
        }
        customerRepository.save(customer);
        return customer;
    }

    public Page<Employee> getManagers(Integer page, Integer size, String role) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return employeeRepository.findEmployeesByRoleRole(role, pageable);
    }

    public Employee getManager(Long id) {
        var manager = employeeRepository.findById(id);
        if (manager.isEmpty()) {
            throw new IllegalArgumentException("Manager not found for ID: " + id);
        }
        if (!manager.get().getRole().getRole().equals("Manager")) {
            throw new IllegalArgumentException("Manager not found for ID: " + id);
        }
        return manager.get();

    }

    public Employee employeeChange(EmployeeChangeByAdminDTO employeeDTO) {
        var employee = employeeRepository.findById(employeeDTO.getId()).get();
        if (employeeDTO.getPassword() != null && !employeeDTO.getPassword().isBlank()) {
            employee.setPassword(encoder.encode(employeeDTO.getPassword()));
        }
        if (!employeeDTO.getFullName().isEmpty()) employee.setFullName(employeeDTO.getFullName());
        if (!employeeDTO.getEmail().isEmpty()) employee.setEmail(employeeDTO.getEmail());
        if (!employeeDTO.getInn().isEmpty()) employee.setInn(employeeDTO.getInn());
        if (!employeeDTO.getLogin().isEmpty()) employee.setLogin(employeeDTO.getLogin());
        if (!employeeDTO.getNumber().isEmpty()) employee.setNumber(employeeDTO.getNumber());
        employeeRepository.save(employee);
        return employee;
    }

    public Employee findEmployee(String email) {
        return employeeRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("Admin not found"));
    }

    public void changeProfileSettings(String username, EmployeeChangeDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getEmail().isEmpty()) employee.setEmail(employeeDTO.getEmail());

        employeeRepository.save(employee);
    }
    public void changeProfileNumberSettings(String username, EmployeeChangeNumberDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getNumber().isEmpty()) employee.setNumber(employeeDTO.getNumber());
        employeeRepository.save(employee);
    }
    public boolean passwordCheck(String username, String prevPassword) {
        Employee employee = findEmployee(username);
        return encoder.matches(prevPassword, employee.getPassword());
    }

    public void changePassword(String username, ChangePasswordDTO changePasswordDTO) {
        Employee employee = findEmployee(username);
        employee.setPassword(encoder.encode(changePasswordDTO.getNewPassword()));
        employeeRepository.save(employee);
    }

    public List<Order> getOrdersByEmployee(String email) {
        return orderRepository.findAllByEmployeesEmail(email);
    }

    public void changeProfileLoginSettings(String username, EmployeeChangeLoginDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getLogin().isEmpty()) employee.setLogin(employeeDTO.getLogin());
        employeeRepository.save(employee);
    }

    public void changeProfileFullNameSettings(String username, EmployeeChangeFullNameDTO employeeDTO) {
        Employee employee = findEmployee(username);
        if (!employeeDTO.getFullName().isEmpty()) employee.setFullName(employeeDTO.getFullName());
        employeeRepository.save(employee);
    }
}
