package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.RegistrationDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RegistrationService {

    private final CustomerRepository customerRepository;
    private final EmployeeRepository employeeRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;

    public boolean registerNewCustomer(RegistrationDTO data, Model model) {
        String email = data.getEmail();
        Optional<Employee> employee = employeeRepository.findByEmail(email);
        if (employee.isPresent()) {
            model.addAttribute("message", "Данный email уже зарегистрирован как Сотрудник!");
            return false;
        }
        Optional<Customer> customer = customerRepository.findByEmail(email);
        if (customer.isPresent()) {
            model.addAttribute("message", "Данный email уже зарегистрирован!");
            return false;
        }
        Role role = roleRepository.findById(2L).orElseThrow();
        customerRepository.save(Customer.builder()
                .email(data.getEmail())
                .password(encoder.encode(data.getPassword()))
                .name(data.getName())
                .company(data.getCompany())
                .number(data.getPhone())
                .enabled(true)
                .ordersQuantity(0)
                .role(role)
                .build());

        return true;
    }
}
