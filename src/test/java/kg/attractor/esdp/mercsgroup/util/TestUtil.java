package kg.attractor.esdp.mercsgroup.util;

import kg.attractor.esdp.mercsgroup.dtos.authorization.GuestRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.ManagerRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.PasswordRecoveryDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.RegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.NewRequestDTO;
import kg.attractor.esdp.mercsgroup.entities.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestUtil {

    public static Order createOrder(String customerEmail){
        return Order.builder()
                .id(1L)
                .customer(createCustomer(customerEmail))
                .employee(createEmployee())
                .orderDate(LocalDateTime.now())
                .lead("lead")
                .leadStart(LocalDateTime.now())
                .leadEnd(LocalDateTime.now().plusDays(15))
                .request(createRequest(customerEmail))
                .status(createStatus())
                .details(new ArrayList<>(Collections.singletonList(createDetail())))
                .build();
    }

    public static Customer createCustomer(String email){
        return Customer.builder()
                .id(1L)
                .role(createRole())
                .customerData(String.valueOf(LocalDateTime.now()))
                .email(email)
                .name("Manager")
                .company("Company")
                .post("Post")
                .number("+996745745745")
                .enabled(true)
                .activity(createActivity())
                .requests(Collections.emptyList())
                .build();
    }

    public static Employee createEmployee(){
        return Employee.builder()
                .id(1L)
                .fullName("Manager1 manager1")
                .role(createRole())
                .login("manager1")
                .email("manager1@mail.ru")
                .inn("22222222222222")
                .post("Manager")
                .number("+996555555555")
                .enabled(true)
                .build();
    }

    public static Request createRequest(String customerEmail){
        return Request.builder()
                .id(1L)
                .customer(createCustomer(customerEmail))
                .product("Кресло")
                .description("Кресло-качалка")
                .requestDate(LocalDateTime.now())
                .status(createStatus())
                .quantity("1")
                .otherInformation("Удобное кресло")
                .delivery(createDelivery())
                .files(Collections.emptyList())
                .build();
    }

    public static Status createStatus(){
        return Status.builder()
                .id(1L)
                .status("В процессе")
                .build();
    }

    public static Detail createDetail(){
        return Detail.builder()
                .id(1L)
                .order(new Order().toBuilder().id(1L).build())
                .productUrl("https://chineseFactory.com/items/78")
                .quantity(1.0)
                .quantityUnit("шт")
                .percentage(10)
                .productPrice(200.0)
                .freight(50.0)
                .customsValue(220.0)
                .finalCost(270.0)
                .build();
    }

    public static Role createRole(){
        return Role.builder()
                .id(1L)
                .role("Manager")
                .build();
    }

    public static Activity createActivity(){
        return Activity.builder()
                .id(1L)
                .activity("HoReCa")
                .build();
    }

    public static Delivery createDelivery(){
        return Delivery.builder()
                .id(1L)
                .delivery("Air")
                .build();
    }

    public static NewRequestDTO createNewRequestDto(){
        NewRequestDTO newRequestDto = new NewRequestDTO();
        newRequestDto.setCustomerId(1L);
        newRequestDto.setProduct("Телефоны");
        newRequestDto.setDescription("Хорошие телефоны");
        newRequestDto.setInformation("С лучшей камерой");
        newRequestDto.setQuantity("100");
        newRequestDto.setDeliveryId(1L);
        return newRequestDto;
    }

    public static Integer getDeclaredGetPageMethod(Integer inputPage, Class<?> className, Object objectClass) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException{
        Method getPageMethod = className.getDeclaredMethod("getPage", Integer.class);
        getPageMethod.setAccessible(true);

        Integer result = (Integer) getPageMethod.invoke(objectClass, inputPage);
        getPageMethod.setAccessible(false);
        return result;
    }

    public static List<Delivery> createDeliveryList(){
        return new ArrayList<>(List.of(
                Delivery.builder().id(1L).delivery("Air").build(),
                Delivery.builder().id(2L).delivery("Auto").build()));
    }

    public static List<GrantedAuthority> createGrantedAuthorityList(String authority){
        return List.of(new SimpleGrantedAuthority(authority));
    }

    public static MockMultipartFile createMockMultipartFile(String fileName, String contentType){
        return new MockMultipartFile(
                "file",
                fileName,
                contentType,
                "some,csv,data".getBytes()
        );
    }

    public static ManagerRegistrationDTO createManagerRegistrationDto(){
        return ManagerRegistrationDTO.builder()
                .email("manager@gmail.com")
                .inn("34533333333333")
                .login("manager")
                .phone("+996723777777")
                .fullName("John")
                .password("12345678")
                .build();
    }

    public static GuestRegistrationDTO createGuestRegistrationDto(){
        return GuestRegistrationDTO.builder()
                .email("guest@gmail.com")
                .name("Tom")
                .phone("+996454232898")
                .company("Google")
                .build();
    }

    public static PasswordRecoveryDTO createPasswordRecoveryDto(){
        return PasswordRecoveryDTO.builder()
                .email("test@gmail.com")
                .password("password")
                .confirmPassword("password")
                .build();
    }

    public static RegistrationDTO createRegistrationDto(){
        return RegistrationDTO.builder()
                .email("test@gmail.com")
                .password("test1234")
                .phone("+996123321231")
                .company("Test company")
                .csrfToken("csrfToken")
                .name("Test")
                .build();
    }

    public static Csrf createCsrfObject(String csrfToken){
        return Csrf.builder()
                .id(1L)
                .csrfToken(csrfToken)
                .createdDateTime(LocalDateTime.now())
                .expirationDateTime(LocalDateTime.now().plusHours(1))
                .employee(TestUtil.createEmployee())
                .build();
    }
}
