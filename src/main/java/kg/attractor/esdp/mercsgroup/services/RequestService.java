package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.request.ChangeRequestDetailsDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.ManagerRequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.NewRequestDTO;
import kg.attractor.esdp.mercsgroup.entities.*;
import kg.attractor.esdp.mercsgroup.repositories.*;
import kg.attractor.esdp.mercsgroup.utils.TelegramUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RequestService {
    private final RequestRepository requestRepository;
    private final RequestFileRepository requestFileRepository;
    private final CustomerRepository customerRepository;
    private final DeliveryRepository deliveryRepository;
    private final RoleRepository roleRepository;
    private final StatusRepository statusRepository;
    private final OrderService orderService;

    public Page<Request> getCustomerRequests(Long customerId, Integer page, Integer size) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return requestRepository.findRequestsByCustomerId(customerId, pageable);
    }

    private Integer getPage(Integer page) {
        if (page != null && page > 0) {
            return page - 1;
        }
        return 0;
    }




    public Page<Request> findAllByStatus(String status, Integer page, Integer size) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return requestRepository.findAllByStatus(status, pageable);
    }

    public void addNewRequest(NewRequestDTO requestDto) throws IOException {
        Optional<Customer> optionalCustomer = customerRepository.getCustomerById(requestDto.getCustomerId());
        if (optionalCustomer.isEmpty()) {
            throw new IllegalArgumentException("Customer not found for ID: " + requestDto.getCustomerId());
        }

        var delivery = deliveryRepository.findById(requestDto.getDeliveryId());
        if (delivery.isEmpty()) {
            throw new IllegalArgumentException("Delivery not found for ID: " + requestDto.getDeliveryId());
        }

        Customer customer = optionalCustomer.get();

        Status status = statusRepository.findStatusByStatus("Новый")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        Request request = Request.builder()
                .customer(customer)
                .product(requestDto.getProduct())
                .description(requestDto.getDescription())
                .quantity(requestDto.getQuantity())
                .otherInformation(requestDto.getInformation())
                .requestDate(LocalDateTime.now())
                .delivery(delivery.get())
                .status(status)
                .build();

        String message = "Была создана новая заявка\n\n" +
                String.format("%-15s %-25s\n", "Покупатель:", request.getCustomer().getName()) +
                String.format("%-19s %-25s\n", "Продукт:", request.getProduct()) +
                String.format("%-15s %-25s\n", "Количество:", request.getQuantity());

        requestRepository.save(request);
        saveFiles(requestDto.getFiles(),request);
        TelegramUtil.sendNotification(message);
    }

    private void saveFiles(MultipartFile[] files, Request request) {
        if (files != null) {
            for (MultipartFile file : files) {
                if (!file.isEmpty()) {
                    saveFile(file, request);
                }
            }
        }
    }


    private void saveFile(MultipartFile file, Request request) {
        String filename = file.getOriginalFilename();
        String baseName = filename.substring(0, filename.lastIndexOf('.'));
        String extension = filename.substring(filename.lastIndexOf('.'));
        int counter = 1;
        String newFilename = filename;

        while (requestFileRepository.existsByFileName(newFilename)) {
            newFilename = baseName + counter + extension;
            counter++;
        }

        Path targetPath = Path.of("storage", newFilename);
        RequestFile requestFile = RequestFile.builder()
                .fileName(newFilename)
                .request(request)
                .build();
        requestFileRepository.save(requestFile);

        try {
            Files.write(targetPath, file.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Request getCustomerRequest(String email, Long id) {
        var request = requestRepository.findById(id);
        if (request.isEmpty()) {
            throw new IllegalArgumentException("По данному коду заявки нету");
        }
        if (!requestRepository.existsByIdAndCustomerEmail(id, email)) {
            throw new IllegalArgumentException("Данная заявка не принадлежит вам");
        }
        return request.get();
    }

    public void addNewManagerRequest(ManagerRequestDTO requestDTO, String employeeEmail) throws IOException {
        var delivery = deliveryRepository.findById(requestDTO.getDeliveryId());
        if (delivery.isEmpty()) {
            throw new IllegalArgumentException("Delivery not found for ID: " + requestDTO.getDeliveryId());
        }

        Optional<Customer> optionalCustomer = customerRepository
                .findByEmailOrNumber(requestDTO.getCustomerEmail(), requestDTO.getCustomerNumber());

        Status status = statusRepository.findStatusByStatus("Новый")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        Request request = Request.builder()
                .product(requestDTO.getProduct())
                .description(requestDTO.getDescription())
                .quantity(requestDTO.getQuantity())
                .otherInformation(requestDTO.getInformation())
                .requestDate(LocalDateTime.now())
                .delivery(delivery.get())
                .status(status)
                .build();

        if (optionalCustomer.isEmpty()) {
            Customer customer = Customer.builder()
                    .name(requestDTO.getCustomerName())
                    .number(requestDTO.getCustomerNumber())
                    .email(requestDTO.getCustomerEmail())
                    .company(requestDTO.getCustomerCompany())
                    .enabled(true)
                    .customerData(requestDTO.getInformation())
                    .ordersQuantity(0)
                    .build();
            if (requestDTO.getCustomerEmail().isEmpty()) {
                customer.setEmail(null);
            }
            Optional<Role> optionalRole = roleRepository.findByRole("Guest");
            optionalRole.ifPresent(customer::setRole);
            customerRepository.save(customer);
            request.setCustomer(customer);
        } else {
            request.setCustomer(optionalCustomer.get());
        }

        requestRepository.save(request);
        saveFiles(requestDTO.getFiles(), request);

        orderService.addNewOrder(request.getId(), employeeEmail);
    }

    public void deleteRequestById(Long id) {
        Request request = requestRepository.findById(id).orElseThrow();
        for (RequestFile file : request.getFiles()) {
            deleteImageInStorageByName(file.getFileName());
        }
        requestRepository.deleteById(id);
    }

    public void changeRequestDetails(ChangeRequestDetailsDTO detailsDTO) {
        Request request = requestRepository.findById(detailsDTO.getIdRequest()).orElse(null);
        if (request != null) {
            request.setProduct(detailsDTO.getProduct());
            request.setOtherInformation(detailsDTO.getOtherInformation());
            request.setQuantity(detailsDTO.getQuantity());
            requestRepository.save(request);
            saveFiles(detailsDTO.getFiles(), request);
        }
    }

    public void deleteImageInStorageByName(String name) {
        String storagePath = "storage";

        String filePath = storagePath + "/" + name;
        File file = new File(filePath);

        if (file.exists()) {
            if (file.delete()) {
                System.out.printf("Файл %s успешно удален.", name);
            } else {
                System.out.printf("Не удалось удалить файл %s.", name);
            }
        } else {
            System.out.printf("Файл %s не существует.", name);
        }
    }

    public void deleteFileByName(String name) {
        RequestFile requestFile = requestFileRepository.findByFileName(name);
        requestFileRepository.deleteById(requestFile.getId());
    }
}
