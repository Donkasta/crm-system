package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findByEmail(String email);

    List<Employee> findEmployeesByRoleRole(String role);
    Page<Employee> findEmployeesByRoleRole(String role, Pageable pageable);

    boolean existsByLogin(String s);
    boolean existsByInn(String s);

    @Query(nativeQuery = true, value = "SELECT CASE WHEN " +
            "exists(SELECT number from customers where number = ?1) " +
            "OR " +
            "exists(SELECT number from employees where number = ?1) " +
            "THEN TRUE ELSE FALSE END")
    boolean existsByNumber(String s);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Employee e SET e.password = :password WHERE e.id = :id")
    void setPasswordById(@Param("password") String password, @Param("id") Long id);

    @Query(nativeQuery = true, value = "SELECT CASE WHEN " +
            "exists(SELECT email from customers where email = ?1) " +
            "OR " +
            "exists(SELECT email from employees where email = ?1) " +
            "THEN TRUE ELSE FALSE END")
    boolean existsByEmail(String email);
}
