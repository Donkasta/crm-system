package kg.attractor.esdp.mercsgroup.dtos.order;

import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.RequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.StatusDTO;
import kg.attractor.esdp.mercsgroup.entities.Order;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class OrderDTO {

    public static OrderDTO from(Order order){
        return builder()
                .id(order.getId())
                .customer(CustomerDTO.from(order.getCustomer()))
                .employee(EmployeeDTO.from(order.getEmployee()))
                .orderDate(Timestamp.valueOf(order.getOrderDate()))
                .request(RequestDTO.from(order.getRequest()))
                .status(StatusDTO.from(order.getStatus()))
                .lead(order.getLead())
                .leadStart(order.getLeadStart() != null ? Timestamp.valueOf(order.getLeadStart()) : null)
                .leadEnd(order.getLeadEnd() != null ? Timestamp.valueOf(order.getLeadEnd()) : null)
                .details(order.getDetails().stream()
                        .map(DetailDTO::from)
                        .collect(Collectors.toList()))
                .build();
    }


    private Long id;
    private CustomerDTO customer;
    private EmployeeDTO employee;
    private Timestamp orderDate;
    private String lead;
    private Timestamp leadStart;
    private Timestamp leadEnd;
    private RequestDTO request;
    private StatusDTO status;
    private List<DetailDTO> details;
}
