package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final EmployeeRepository employeeRepository;
    private final CustomerRepository customerRepository;

    public String getProfilePage(String email){
        Optional<Employee> employee = employeeRepository.findByEmail(email);
        if (employee.isPresent()) {
            if(employee.get().getRole().getRole().equals("Manager"))
                return "redirect:/employees/profile";
            return "redirect:/admin/profile";
        }

        Optional<Customer> customer = customerRepository.findByEmail(email);
        if (customer.isPresent()) {
            return "redirect:/customers/profile";
        }

        throw new UsernameNotFoundException("User with this email not found");
    }
}
