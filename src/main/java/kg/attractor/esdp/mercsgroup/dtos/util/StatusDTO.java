package kg.attractor.esdp.mercsgroup.dtos.util;

import kg.attractor.esdp.mercsgroup.entities.Status;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatusDTO {

    public static StatusDTO from(Status status){
        return builder()
                .id(status.getId())
                .status(status.getStatus())
                .build();

    }

    private Long id;

    private String status;
}
