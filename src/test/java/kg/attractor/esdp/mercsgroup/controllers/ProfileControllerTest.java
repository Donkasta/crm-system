package kg.attractor.esdp.mercsgroup.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProfileControllerTest {

    @Autowired
    private ProfileController profileController;

    @Test
    public void testGetLoginPage_WithNullErrorParam_ReturnsLoginPage() {
        Model model = new ConcurrentModel();
        String response = profileController.getLoginPage(null, model, null);
        String expectedResponse = "login";

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetLoginPage_WithAuthenticatedUser_RedirectsToHomePage() {
        String password = "12345678";
        Model model = new ConcurrentModel();
        UserDetails userDetails = new User("cr1@mail.ru", password, Collections.emptyList());
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, password);

        String response = profileController.getLoginPage(null, model, authentication);
        String expectedResponse = "redirect:/";

        assertEquals(expectedResponse, response);
    }
}