package kg.attractor.esdp.mercsgroup.controllers;

import com.opencsv.exceptions.CsvException;
import kg.attractor.esdp.mercsgroup.services.CSVService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Controller
@RequestMapping("/csv")
@RequiredArgsConstructor
public class CSVController {
    private final CSVService csvService;

    @PostMapping
    public ResponseEntity<String> uploadCSVFile(@RequestParam("file") MultipartFile file) throws IOException, CsvException,
            DataIntegrityViolationException {
        if(csvService.uploadCSVFile(file)){
            return ResponseEntity.ok("Вы успешно загрузили файл");
        }
        return ResponseEntity.badRequest().body("Не удалось загрузить файл");
    }

    @ExceptionHandler(IOException.class)
    public String handleIOException(IOException ex, Model model) {
        model.addAttribute("message", "Ошибка в процессе записи CSV файла" + ex.getMessage());
        return "error/errorMessage";
    }

    @ExceptionHandler(CsvException.class)
    public String handleCsvException(CsvException ex, Model model) {
        model.addAttribute("message", "Ошибка в процессе записи CSV файла: " + ex.getMessage());
        return "error/errorMessage";
    }
}
