package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.PasswordRecoveryDTO;
import kg.attractor.esdp.mercsgroup.services.PasswordRecoveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Controller
@RequiredArgsConstructor
@RequestMapping("/password-recovery")
public class PasswordRecoveryController {
    private final PasswordRecoveryService passwordRecoveryService;

    @GetMapping("/specify-email")
    public String getSpecifyEmailPage(){
        return "password-recovery-specifying-email";
    }

    @PostMapping("/specify-email")
    public String getTokenConfirmationPage(Model model,
                                           @Valid @RequestParam("username")
                                           @NotBlank(message = "Введите ваш email")
                                           @Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$", message = "Некорректный адрес электронной почты")
                                           @Email String email) {
        boolean bool = passwordRecoveryService.sendPasswordRecoveryTokenToEmail(email);
        if(bool){
            model.addAttribute("email", email);
            return "password-recovery-token-confirmation";
        } else {
            return "redirect:/login";
        }
    }

    @PostMapping("/token-confirmation")
    public String confirmToken(Model model,
                               @Valid @RequestParam("username") @NotBlank(message = "Введите ваш email")
                               @Email String email,
                               @Valid @RequestParam("token") @NotBlank(message = "Введите токен") String token,
                               @RequestParam(required = false) String error){
        boolean bool = passwordRecoveryService.confirmToken(email, token);
        if(bool){
            model.addAttribute("email", email);
            model.addAttribute("error", error != null);
            return "password-recovery";
        } else {
            return "redirect:/password-recovery/specify-email";
        }
    }

    @PostMapping()
    public String recoverPassword(@Valid PasswordRecoveryDTO data, BindingResult bindingResult,
                                  Model model, @RequestParam(required = false) String error){
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
            model.addAttribute("email", data.getEmail());
            model.addAttribute("error", error != null);
            return "password-recovery";
        } else {
            passwordRecoveryService.recoverPassword(data);
            return "redirect:/login";
        }
    }
}
