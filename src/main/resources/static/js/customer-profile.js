'use strict';

const BASIC_URL = '/customers/';
let CURRENT_PAGE = 0;
let SORT_ORDER = -1;

function change(id) {
    const elements = ['information', 'orders', 'payments', 'settings', 'requests', 'new-request'];
    elements.forEach(elementId => {
        const element = document.getElementById(elementId);
        element.style.display = elementId === id ? 'block' : 'none';
    });
}
const currentURL = window.location.href;

if (currentURL.includes("/?error")) {
    change('settings')
}else {
    getCurrentBlock('orders')
}

async function getCurrentBlockAndClearLS(type){
    localStorage.clear();
    await getCurrentBlock(type);
}

async function getCurrentBlock(type){
    const response = await fetch(`${BASIC_URL}${type}-replace`);
    const html = await response.text();
    if (ifLogin(html)) {
        window.location.href = "/login";
        return;
    }
    const block = document.getElementById(`${type}`);
    block.innerHTML = html;
    change(`${type}`)
}

function ifLogin(html) {
    const block = document.createElement('div');
    block.innerHTML = html;
    const itsLoginElement = block.querySelector('#itsLogin');
    return !!itsLoginElement;
}


async function changeOrdersPage(page, count) {
    CURRENT_PAGE = page;
    if (count > 0){
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }

    const newPage = parseInt(page)+count;

    let productValue = localStorage.getItem('productValue') || '';
    let descriptionValue =  localStorage.getItem('descriptionValue') || '';
    let statusValue =  localStorage.getItem('statusValue') || '';
    let param =  localStorage.getItem('sortParam') || '';

    const response = await fetch(`${BASIC_URL}orders-replace?sortBy=${param}&sortOrder=${SORT_ORDER}&productName=${productValue}&description=${descriptionValue}
    &status=${statusValue}&page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById(`orders`);
    block.innerHTML = html;
}

async function changePage(page, count, type) {
    CURRENT_PAGE = page;
    if (count > 0){
        CURRENT_PAGE += 1;
    } else {
        CURRENT_PAGE -= 1;
    }

    const newPage = parseInt(page)+count;
    const response = await fetch(`${BASIC_URL}${type}-replace?page=${newPage}`);
    const html = await response.text();
    const block = document.getElementById(`${type}`);
    block.innerHTML = html;
}

async function searchRequests(event){
    event.preventDefault();

    const form = document.getElementById('searchForm');
    const productInput = form.elements.productName;
    const descriptionInput = form.elements.description;
    const statusInput = form.elements.status;
    const type = 'orders';
    const productValue = productInput.value;
    const descriptionValue = descriptionInput.value;
    const statusValue = statusInput.value;

    const formCheckbox = document.getElementById("sortForm");
    if (formCheckbox) {
        const checkboxes = formCheckbox.getElementsByTagName("input");
        const selectedCheckboxes = [];
        for (let i = 0; i < checkboxes.length; i++) {
            const checkbox = checkboxes[i];
            if (checkbox.type === "checkbox" && checkbox.checked) {
                selectedCheckboxes.push(checkbox.value);
            }
        }
        await sendRequestToSearchRequests(productValue, descriptionValue, statusValue, selectedCheckboxes, type);
    } else {
        const selectedCheckboxes = [];
        await sendRequestToSearchRequests(productValue, descriptionValue, statusValue, selectedCheckboxes, type);
    }
}




function showDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'block';
}

function closeDisplayModal() {
    const id = document.getElementById('modal-exit');
    id.style.display = 'none';
}

function openOrderModal(element) {
    let modal = new bootstrap.Modal(document.querySelector(`#${element}`));
    modal.show();

    let closeButton = document.querySelector(`#${element + '-btn-close'}`)
    closeButton.addEventListener('click', () => {
        modal.hide();
    });
}


const newRequest = document.getElementById('new-request');
newRequest.addEventListener('submit',addNewRequest);

const csrfHeader = document.querySelector('meta[name="_csrf_header"]').content;
const csrfToken = document.querySelector('meta[name="_csrf"]').content;

async function addNewRequest(e) {
    e.preventDefault();
    const data = new FormData(e.target);
    const response = await fetch('/requests/new', {
        method: 'POST',
        headers: {
            [csrfHeader]: csrfToken
        },
        body: data
    });

    if (response.ok) {
        let responseField = document.getElementById('response-field')
        responseField.classList.remove('d-none')
    } else {
        const errorResponse = await response.json();
        if (errorResponse && errorResponse.errors) {
            const errorMessages = errorResponse.errors;
            errorMessages.forEach((error) => {
                let message = error.defaultMessage;
                let field = error.field;
                let errorField = document.getElementById(`${field}-error`)
                errorField.innerHTML = message
                errorField.classList.remove('d-none')
            })
        } else {
            alert('Произошла ошибка');
        }
    }

    e.target.reset();
}

const maxFiles = 10;

let fileCounter = 1;

function addFileInput() {
    fileCounter++;
    const newFileInput = document.createElement("input");
    newFileInput.type = "file";
    newFileInput.name = "files[]";
    newFileInput.className = "form-control my-2";
    const label = document.createElement("label");
    label.innerText = `Выберите файл №:`+fileCounter;
    label.className = "form-label";

    const container = document.getElementById("attach-file");
    container.appendChild(label);
    container.appendChild(newFileInput);

    // Если достигли максимального количества файлов, скрываем кнопку
    if (fileCounter === maxFiles) {
        document.getElementById("add-attachment-button").style.display = "none";
    }
}

function closeOrOpenCustomerModal(id) {
    const modal = document.getElementById(id);
    if (modal.style.display === 'none') {
        modal.style.display = 'block';
    } else {
        modal.style.display = 'none';
    }
}

function showCustomConfirm() {
    const customConfirm = document.getElementById('confirmOffer');
    const confirmBtn = document.getElementById('confirmBtn');
    const cancelBtnTop = document.getElementById('cancelBtnTop');
    const cancelBtn = document.getElementById('cancelBtn');

    customConfirm.style.display = 'flex';

    return new Promise((resolve) => {
        function confirmAction(confirm) {
            customConfirm.style.display = 'none';
            resolve(confirm);
        }

        confirmBtn.onclick = () => confirmAction(true);
        cancelBtn.onclick = () => confirmAction(false);
        cancelBtnTop.onclick = () => confirmAction(false);
    });
}

async function changeStatus(orderId) {
    showCustomConfirm(orderId).then((confirmed) => {
        if (confirmed) {
            updateOrderStatus(orderId);
        }
    });
}

async function updateOrderStatus(orderId) {

    let csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
    let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");

    const data = {
        orderId: orderId
    };


    fetch('/orders/status/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            if (response.ok) {
                getCurrentBlock('orders')
            } else {
                console.error('Error:', response.statusText);
            }
        })
}

async function sendRequestToSearchRequests(productValue, descriptionValue, statusValue, selectedCheckboxes, type){
    const encodeProductValue = encodeURIComponent(productValue);
    const encodeDescriptionValue = encodeURIComponent(descriptionValue);

    localStorage.setItem('productValue', encodeProductValue);
    localStorage.setItem('descriptionValue', encodeDescriptionValue);
    localStorage.setItem('statusValue', statusValue);

    const url = `/customers/orders-replace/search-sort?productName=${encodeProductValue}&description=${encodeDescriptionValue}
    &status=${statusValue}`;
    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById(`${type}`);
    block.innerHTML = html;
    change(`${type}`)
}


const sortBy = async (param) => {
    if (SORT_ORDER === 1){
        SORT_ORDER = -1;
    } else {
        SORT_ORDER = 1;
    }

    localStorage.setItem('sortParam', param);

    let productValue = localStorage.getItem('productValue') || '';
    let descriptionValue =  localStorage.getItem('descriptionValue') || '';
    let statusValue =  localStorage.getItem('statusValue') || '';


    const url = `${BASIC_URL}orders-replace?sortBy=${param}&sortOrder=${SORT_ORDER}&productName=${productValue}&description=${descriptionValue}
    &status=${statusValue}`;


    const response = await fetch(url);
    const html = await response.text();
    const block = document.getElementById('orders');
    block.innerHTML = html;
}