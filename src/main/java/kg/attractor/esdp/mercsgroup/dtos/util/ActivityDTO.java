package kg.attractor.esdp.mercsgroup.dtos.util;

import kg.attractor.esdp.mercsgroup.entities.Activity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ActivityDTO {
    public static ActivityDTO from(Activity activity){
        return builder()
                .id(activity.getId())
                .activity(activity.getActivity())
                .build();
    }

    private Long id;
    private String activity;
}
