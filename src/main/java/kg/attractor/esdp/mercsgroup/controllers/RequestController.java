package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.ChangeRequestDetailsDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.NewRequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.DeliveryDTO;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.DeliveryService;
import kg.attractor.esdp.mercsgroup.services.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.io.IOException;
import java.util.stream.Collectors;


@Controller
@RequiredArgsConstructor
@RequestMapping("/requests")
public class RequestController {
    private final RequestService requestService;
    private final CustomerService customerService;
    private final DeliveryService deliveryService;

    @GetMapping("/new")
    public String getNewRequest(
            Model model,
            Authentication authentication,
            @RequestParam(required = false) String message,
            @RequestParam(required = false) String error
    ) {
        var customer = CustomerDTO.from(customerService.findCustomer(authentication.getName()));
        model.addAttribute("customer", customer);
        model.addAttribute("message", message != null);
        model.addAttribute("error", error != null);
        var deliveries = deliveryService.getAll().stream()
                .map(DeliveryDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("deliveries", deliveries);
        return "add-new-request";
    }

    @PostMapping("/new")
    public Object addNewRequest(
            @Valid NewRequestDTO request,
            Model model,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
            return "add-new-request";
        }
        try {
            requestService.addNewRequest(request);
            return ResponseEntity.ok("Добавлено");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Ошибка при добавлении заявки: " + e.getMessage());
        }
    }

    @PostMapping("/delete")
    public ResponseEntity<String> deleteRequestById(@RequestParam("id") Long id) {
        requestService.deleteRequestById(id);
        return ResponseEntity.ok("Успешно удалено");
    }

    @PostMapping("/change")
    public Object changeRequestDetails(@Valid ChangeRequestDetailsDTO detailsDTO, Model model,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
            ResponseEntity.badRequest().body("Ошибка заполните поля");
        }
        requestService.changeRequestDetails(detailsDTO);
        return ResponseEntity.ok("Успешно Изменен");
    }

    @PostMapping("/delete/img")
    public ResponseEntity<String> deleteImgByName(@RequestParam("name") String name) {
        String[] array = name.split(",");
        for (String s : array) {
            requestService.deleteFileByName(s);
            requestService.deleteImageInStorageByName(s);
        }
        return ResponseEntity.ok("Успешно удален");
    }

}
