package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.GuestRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.ManagerRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeChangeNumberDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.RoleRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@SpringBootTest
public class AdminServiceTest {
    private static final String EMAIL = "admin@mail.ru";
    private static final String PASSWORD = "12345678";

    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    EmployeeRepository employeeRepository;

    @MockBean
    CustomerRepository customerRepository;

    @MockBean
    RoleRepository roleRepository;
    @Autowired
    private AdminService adminService;

    @BeforeEach
    void setup() {
        Employee employee = createEmployee(EMAIL);
        when(employeeRepository.findByEmail(employee.getEmail())).thenReturn(Optional.of(employee));
        when(passwordEncoder.matches(eq(PASSWORD), anyString())).thenReturn(true);
    }

    @Test
    void testFindEmployee_ExistingEmployee() {
        String username = EMAIL;
        Employee optionalEmployee = createEmployee(username);
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(optionalEmployee));

        Employee employee = adminService.findEmployee(username);

        assertAll(
                () -> assertNotNull(employee),
                () -> assertEquals(username, employee.getEmail())
        );
    }

    @Test
    void testFindEmployee_NonExistingEmployee() {
        String username = EMAIL;
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> adminService.findEmployee(username));
    }

    @Test
    void testPasswordCheck_ValidPassword()  {
        String username = EMAIL;
        String prevPassword = PASSWORD;

        Employee employee = createEmployee(username);

        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));
        when(passwordEncoder.matches(prevPassword, employee.getPassword()))
                .thenAnswer(invocationOnMock -> prevPassword.equals(employee.getPassword()));

        boolean result = adminService.passwordCheck(username, prevPassword);

        assertTrue(result);
    }

    @Test
    void testChangePassword_PasswordChangeSuccessful() {
        String username = EMAIL;
        String prevPassword = PASSWORD;

        Employee initEmployee = createEmployee(username);

        ChangePasswordDTO changePasswordDTO = ChangePasswordDTO.builder()
                .prevPassword(prevPassword)
                .newPassword(prevPassword)
                .newPasswordRepeat(prevPassword)
                .build();
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(initEmployee));
        adminService.changePassword(username, changePasswordDTO);

        verify(employeeRepository, times(1)).save(any(Employee.class));
    }

    private Employee createEmployee(String username){
        Employee employee = new Employee();
        employee.setEmail(username);
        employee.setFullName("Admin admin");
        employee.setNumber("+996777888998");
        employee.setInn("44444444444444");
        employee.setPost("Administrator");
        employee.setLogin("admin");
        employee.setPassword("12345678");
        return employee;
    }

    @Test
    void testChangeProfileSettings_ProfileSettingsChangeSuccessful(){
        String username = EMAIL;
        EmployeeChangeDTO employeeDTO = new EmployeeChangeDTO();
        employeeDTO.setEmail("email@gmail.com");

        Employee employee = createEmployee(username);
        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));

        adminService.changeProfileSettings(username, employeeDTO);

        verify(employeeRepository, times(1)).save(employee);
        assertEquals(employeeDTO.getEmail(), employee.getEmail());
    }

    @Test
    void testChangeProfileNumberSettings_NumberChangeSuccessful(){
        String username = "manager1@mail.ru";
        Employee employee = createEmployee(username);
        EmployeeChangeNumberDTO employeeChangeNumberDTO = new EmployeeChangeNumberDTO();
        employeeChangeNumberDTO.setNumber("+996654654654");

        when(employeeRepository.findByEmail(username)).thenReturn(Optional.of(employee));

        adminService.changeProfileNumberSettings(username, employeeChangeNumberDTO);

        verify(employeeRepository, times(1)).save(employee);
        assertEquals(employeeChangeNumberDTO.getNumber(), employee.getNumber());
    }

    @Test
    void testGetCustomersWithValidRoleAndDefaultPageAndSize(){
        String role = "Customer";
        searchClientsByRole(role);
    }

    @Test
    void testGetGuestsWithValidRoleAndDefaultPageAndSize(){
        String role = "Guest";
        searchClientsByRole(role);
    }

    @Test
    void testGetEmployeesWithValidRoleAndDefaultPageAndSize(){
        String role = "Manager";
        int defaultPage = 0;
        int defaultSize = 5;

        Pageable pageable = PageRequest.of(defaultPage, defaultSize);
        Page<Employee> page = new PageImpl<>(List.of(TestUtil.createEmployee()));
        when(employeeRepository.findEmployeesByRoleRole(role, pageable))
                .thenReturn(page);
        Page<Employee> response = adminService.getManagers(defaultPage, defaultSize, role);

        verify(employeeRepository, times(1)).findEmployeesByRoleRole(role, pageable);
        assertEquals(page, response);
    }

    private void searchClientsByRole(String role){
        int defaultPage = 0;
        int defaultSize = 5;


        Pageable pageable = PageRequest.of(defaultPage, defaultSize);
        Page<Customer> page = new PageImpl<>(List.of(TestUtil.createCustomer(EMAIL)));
        when(customerRepository.findCustomersByRoleRole(role, pageable))
                .thenReturn(page);
        Page<Customer> response = adminService.getCustomers(defaultPage, defaultSize, role);

        verify(customerRepository, times(1)).findCustomersByRoleRole(role, pageable);
        assertEquals(page, response);
    }

    @Test
    void testGetPage_PositivePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException  {
        int inputPage = 3;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, AdminService.class, adminService);
        assertEquals(inputPage - 1, result);
    }

    @Test
    public void testGetPage_NegativePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        int inputPage = -50;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, AdminService.class, adminService);
        assertEquals(0, result);
    }

    @Test
    public void testRegisterNewManager_EmployeeFoundBehavior() {
        ManagerRegistrationDTO managerRegistrationDTO = TestUtil.createManagerRegistrationDto();
        Model model = new ConcurrentModel();
        Employee employee = TestUtil.createEmployee();
        when(employeeRepository.findByEmail(managerRegistrationDTO.getEmail())).thenReturn(Optional.of(employee));
        boolean response = adminService.registerNewManager(managerRegistrationDTO, model);
        registerForEmployeeFoundBehavior(response, model);
    }

    @Test
    public void testRegisterNewManager_CustomerFoundBehavior(){
        ManagerRegistrationDTO managerRegistrationDTO = TestUtil.createManagerRegistrationDto();
        Model model = new ConcurrentModel();
        Customer customer = TestUtil.createCustomer("cr@mail.ru");
        when(employeeRepository.findByEmail(managerRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(managerRegistrationDTO.getEmail())).thenReturn(Optional.of(customer));
        boolean response = adminService.registerNewManager(managerRegistrationDTO, model);
        registerForCustomerFoundBehavior(response, model);
    }

    @Test
    public void testRegisterNewManager_UniqueEmailRegistration() {
        ManagerRegistrationDTO managerRegistrationDTO = TestUtil.createManagerRegistrationDto();
        Model model = new ConcurrentModel();
        Role role = TestUtil.createRole();
        when(employeeRepository.findByEmail(managerRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(managerRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(roleRepository.findById(1L)).thenReturn(Optional.of(role));
        boolean response = adminService.registerNewManager(managerRegistrationDTO, model);
        assertTrue(response);
        verify(employeeRepository, times(1)).save(any());
    }

    @Test
    public void testRegisterNewGuest_EmployeeFoundBehavior() {
        GuestRegistrationDTO guestRegistrationDTO = TestUtil.createGuestRegistrationDto();
        Model model = new ConcurrentModel();
        Employee employee = TestUtil.createEmployee();
        when(employeeRepository.findByEmail(guestRegistrationDTO.getEmail())).thenReturn(Optional.of(employee));
        boolean response = adminService.registerNewGuest(guestRegistrationDTO, model);
        registerForEmployeeFoundBehavior(response, model);
    }

    private void registerForEmployeeFoundBehavior(boolean response, Model model){
        assertFalse(response);
        String expectedModelResponse = "Данный email уже зарегистрирован как Сотрудник!";
        assertEquals(expectedModelResponse, model.getAttribute("message"));
    }

    @Test
    public void testRegisterNewGuest_CustomerFoundBehavior(){
        GuestRegistrationDTO guestRegistrationDTO = TestUtil.createGuestRegistrationDto();
        Model model = new ConcurrentModel();
        Customer customer = TestUtil.createCustomer("cr@mail.ru");
        when(employeeRepository.findByEmail(guestRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(guestRegistrationDTO.getEmail())).thenReturn(Optional.of(customer));
        boolean response = adminService.registerNewGuest(guestRegistrationDTO, model);
        registerForCustomerFoundBehavior(response, model);
    }

    private void registerForCustomerFoundBehavior(boolean response, Model model){
        assertFalse(response);
        String expectedModelResponse = "Данный email уже зарегистрирован!";
        assertEquals(expectedModelResponse, model.getAttribute("message"));
    }

    @Test
    public void testRegisterNewGuest_UniqueEmailRegistration() {
        GuestRegistrationDTO guestRegistrationDTO = TestUtil.createGuestRegistrationDto();
        Model model = new ConcurrentModel();
        Role role = TestUtil.createRole();
        when(employeeRepository.findByEmail(guestRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(guestRegistrationDTO.getEmail())).thenReturn(Optional.empty());
        when(roleRepository.findById(4L)).thenReturn(Optional.of(role));
        boolean response = adminService.registerNewGuest(guestRegistrationDTO, model);
        assertTrue(response);
        verify(customerRepository, times(1)).save(any());
    }
}