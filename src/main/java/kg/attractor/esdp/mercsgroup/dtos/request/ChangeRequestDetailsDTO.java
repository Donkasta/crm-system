package kg.attractor.esdp.mercsgroup.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class ChangeRequestDetailsDTO {
    private Long idRequest;
    @NotBlank(message = "Заполните поле")
    private String product;
    @NotNull(message = "Заполните поле")
    private String quantity;
    private String otherInformation;
    private MultipartFile[] files;
}
