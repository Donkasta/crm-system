package kg.attractor.esdp.mercsgroup.dtos.request;


import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.StatusDTO;
import kg.attractor.esdp.mercsgroup.entities.Request;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class RequestDTO {

    public static RequestDTO from(Request request){
        return builder()
                .id(request.getId())
                .product(request.getProduct())
                .customer(CustomerDTO.from(request.getCustomer()))
                .description(request.getDescription())
                .requestDate(Timestamp.valueOf(request.getRequestDate()))
                .quantity(request.getQuantity())
                .otherInformation(request.getOtherInformation())
                .files(request.getFiles().stream()
                        .map(RequestFileDTO::from)
                        .collect(Collectors.toList()))
                .status(StatusDTO.from(request.getStatus()))
                .build();
    }
    public static RequestDTO fromWithoutCustomer(Request request){
        return builder()
                .product(request.getProduct())
                .id(request.getId())
                .requestDate(Timestamp.valueOf(request.getRequestDate()))
                .quantity(request.getQuantity())
                .description(request.getDescription())
                .files(request.getFiles().stream()
                        .map(RequestFileDTO::from)
                        .collect(Collectors.toList()))
                .status(StatusDTO.from(request.getStatus()))
                .build();
    }


    private Long id;
    private String product;
    private CustomerDTO customer;
    private Timestamp requestDate;
    private String description;
    private String quantity;
    private String otherInformation;
    private List<RequestFileDTO> files;
    private StatusDTO status;



}
