package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Csrf;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

public interface CsrfRepository extends JpaRepository<Csrf, Long> {
    Optional<Csrf> findByCsrfToken(String csrf);

    @Modifying
    @Transactional
    void deleteAllByExpirationDateTimeBefore(LocalDateTime expirationDateTime);
}