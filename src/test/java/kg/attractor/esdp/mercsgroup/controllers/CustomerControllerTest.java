package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.DeliveryDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Delivery;
import kg.attractor.esdp.mercsgroup.entities.Request;
import kg.attractor.esdp.mercsgroup.services.CustomerService;
import kg.attractor.esdp.mercsgroup.services.DeliveryService;
import kg.attractor.esdp.mercsgroup.services.RequestService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private DeliveryService deliveryService;

    @MockBean
    private RequestService requestService;

    @Autowired
    private CustomerController customerController;

    private static final String EMAIL = "cr2@mail.ru";
    private static final String PASSWORD = "12345678";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testGetCustomerNewRequestReplace() throws Exception {
        Customer customer = TestUtil.createCustomer(EMAIL);
        List<Delivery> deliveryList = List.of(TestUtil.createDelivery());

        Authentication authentication = new UsernamePasswordAuthenticationToken(EMAIL, PASSWORD);

        when(customerService.findCustomer(EMAIL)).thenReturn(customer);
        when(deliveryService.getAll()).thenReturn(deliveryList);


        mockMvc.perform(get("/customers/new-request-replace")
                .with(csrf())
                .principal(authentication))
                .andExpect(status().isOk())
                .andExpect(view().name("/block-replacement/customer-new-request-replace"))
                .andExpect(model().attributeExists("customer", "message", "error", "deliveries"))
                .andExpect(model().attribute("customer", CustomerDTO.from(customer)))
                .andExpect(model().attribute("message", false))
                .andExpect(model().attribute("error", false))
                .andExpect(model().attribute("deliveries",
                    deliveryList.stream().map(DeliveryDTO::from).collect(Collectors.toList())));

        verify(customerService, times(1)).findCustomer(EMAIL);
        verify(deliveryService, times(1)).getAll();
    }

    @Test
    @WithMockUser(username = EMAIL, password = PASSWORD)
    void testReplaceRequests_Successful() throws Exception{
        Customer customer = TestUtil.createCustomer(EMAIL);
        int page = 1;
        int size = 5;
        List<Request> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Request request = TestUtil.createRequest(EMAIL);
            request.setId(i + 1L);
            list.add(request);
        }
        Pageable pageable = PageRequest.of(page, size);
        Page<Request> requestPage = new PageImpl<>(list, pageable, list.size());

        when(customerService.findCustomer(EMAIL)).thenReturn(customer);
        when(requestService.getCustomerRequests(customer.getId(), page, size)).thenReturn(requestPage);

        mockMvc.perform(get("/customers/requests-replace")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("block-replacement/customer-request-replace"));

        verify(customerService, times(1)).findCustomer(EMAIL);
        verify(requestService, times(1)).getCustomerRequests(customer.getId(), page, size);
    }

    @Test
    void testGetCustomerRequest_Successful() {
        Request request = TestUtil.createRequest(EMAIL);
        UserDetails userDetails = new User(EMAIL, PASSWORD, Collections.emptyList());
        Model model = new ConcurrentModel();
        when(requestService.getCustomerRequest(EMAIL, request.getId())).thenReturn(request);

        customerController.getCustomerRequest(userDetails, model, request.getId());

        verify(requestService, times(1)).getCustomerRequest(EMAIL, request.getId());
    }
}