package kg.attractor.esdp.mercsgroup.controllers;


import kg.attractor.esdp.mercsgroup.dtos.authorization.ChangePasswordDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.GuestRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.authorization.ManagerRegistrationDTO;
import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.*;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.services.AdminService;
import kg.attractor.esdp.mercsgroup.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin")
public class AdminController {

    private final AdminService adminService;

    private final OrderService orderService;

    @GetMapping("employee-registration")
    public String managerRegistration(){

        return "/admin/manager-registration";
    }

    @PostMapping("/profile/changes/email")
    public String applyCustomerProfileChanges(@AuthenticationPrincipal UserDetails userDetails,
                                              @Valid EmployeeChangeDTO employeeDTO, BindingResult bindingResult,
                                              RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        String username = userDetails.getUsername();
        adminService.changeProfileSettings(username, employeeDTO);

        return "redirect:/";
    }

    @PostMapping("/profile/changes/full-name")
    public String applyCustomerProfileFullNameChanges(@AuthenticationPrincipal UserDetails userDetails,
                                                   @Valid EmployeeChangeFullNameDTO employeeDTO, BindingResult bindingResult,
                                                   RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        String username = userDetails.getUsername();
        adminService.changeProfileFullNameSettings(username, employeeDTO);

        return "redirect:/";
    }

    @PostMapping("/profile/changes/login")
    public String applyCustomerProfileLoginChanges(@AuthenticationPrincipal UserDetails userDetails,
                                                   @Valid EmployeeChangeLoginDTO employeeDTO, BindingResult bindingResult,
                                                   RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        String username = userDetails.getUsername();
        adminService.changeProfileLoginSettings(username, employeeDTO);

        return "redirect:/";
    }

    @PostMapping("/profile/changes/number")
    public String applyCustomerProfileNumberChanges(@AuthenticationPrincipal UserDetails userDetails,
                                                    @Valid EmployeeChangeNumberDTO employeeDTO, BindingResult bindingResult,
                                                    RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        String username = userDetails.getUsername();
        adminService.changeProfileNumberSettings(username, employeeDTO);

        return "redirect:/";
    }

    @PostMapping("/profile/changePassword")
    public String changePassword(@AuthenticationPrincipal UserDetails userDetails,
                                 @Valid ChangePasswordDTO changePasswordDTO,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        String username = userDetails.getUsername();
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/?error";
        }

        if (changePasswordDTO.getNewPassword().equals(changePasswordDTO.getNewPasswordRepeat())
                && adminService.passwordCheck(username, changePasswordDTO.getPrevPassword())) {
            adminService.changePassword(username, changePasswordDTO);
            redirectAttributes.addFlashAttribute("passwordChanged", true);
        } else {
            redirectAttributes.addFlashAttribute("passwordError", true);
        }

        return "redirect:/?error";
    }

    @PostMapping("/employee-registration")
    public String employeeRegistration(@Valid ManagerRegistrationDTO data, BindingResult bindingResult,
                                       Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
        } else {
            if (adminService.registerNewManager(data, model)) {
                return "redirect:/";
            }
        }
        return "/admin/manager-registration";
    }

    @GetMapping("/employees/{id}")
    public String getEmployee(@PathVariable Long id,Model model){
        var manager = EmployeeDTO.from(adminService.getManager(id));
        var orders = adminService.getOrdersByEmployee(manager.getEmail())
                .stream()
                .map(OrderDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("orders", orders);
        model.addAttribute("manager", manager);
        return "/admin/admin-employee-orders";
    }

    @GetMapping("/orders/{id}")
    public String getOrder(@PathVariable Long id,Model model){

        var order = OrderDTO.from(orderService.getOrderById(id));
        model.addAttribute("order", order);

        return "/admin/admin-order";
    }


    @GetMapping("/employees-edit/{id}")
    public String employeeEdit(@PathVariable Long id,Model model){
        var manager = adminService.getManager(id);
        model.addAttribute("manager",manager);
        return "/admin/admin-employee-edit";
    }

    @PostMapping("/employee")
    public String employeeEdit(@Valid EmployeeChangeByAdminDTO employeeDTO, Model model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
            var manager = adminService.getManager(employeeDTO.getId());
            model.addAttribute("manager", manager);
        } else {
            var employee = EmployeeChangeByAdminDTO.from(adminService.employeeChange(employeeDTO));
            model.addAttribute("manager", employee);
        }
        return "/admin/admin-employee-edit";
    }
    @GetMapping("/guests/{id}")
    public String getGuest(@PathVariable Long id, Model model) {
        var guest = CustomerDTO.from(adminService.getGuest(id));
        model.addAttribute("guest", guest);
        var managers = adminService.getManagersForGuest().stream()
                .map(EmployeeDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("managers",managers);
        return "/admin/admin-guest";
    }

    @PostMapping("/guest")
    public String guestChanges(CustomerDTO customerDTO, Model model) {
        var guest = CustomerDTO.from(adminService.guestChange(customerDTO));
        model.addAttribute("guest", guest);
        var managers = adminService.getManagersForGuest().stream()
                .map(EmployeeDTO::from)
                .collect(Collectors.toList());
        model.addAttribute("managers",managers);
        return "/admin/admin-guest";
    }

    @GetMapping("/customers/{id}")
    public String getCustomer(@PathVariable Long id, Model model) {
        var customer = CustomerDTO.from(adminService.getCustomer(id));
        model.addAttribute("customer", customer);
        return "/admin/admin-customer";
    }

    @PostMapping("/customer")
    public String customerChanges(@Valid CustomerDTO customerDTO,
                                  BindingResult bindingResult,
                                  RedirectAttributes redirectAttributes,
                                  Model model) {
        if (!adminService.emailIsUnique(customerDTO.getEmail())) {
            redirectAttributes.addFlashAttribute("emailError", "Пользователь с таким email уже существует");
            return "redirect:/admin/customers/" + customerDTO.getId();
        }
        var customer = CustomerDTO.from(adminService.customerChange(customerDTO));
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/admin/customers/" + customer.getId();
        }
        model.addAttribute("customer", customer);
        return "/admin/admin-customer";
    }

    @GetMapping("/customers-replace")
    public String getCustomers(
            Model model,
            @RequestParam(defaultValue = "1", required = false) Integer page,
            @RequestParam(defaultValue = "5", required = false) Integer size
    ) {
        var pageable = adminService.getCustomers(page, size, "Customer");
        var customersList = pageable.stream()
                .map(CustomerDTO::from)
                .collect(Collectors.toList());
        Page<CustomerDTO> customers = new PageImpl<>(customersList, pageable.getPageable(), pageable.getTotalElements());
        model.addAttribute("customers", customers);
        return "/admin/block-replacement/customers-replace";
    }

    @GetMapping("/guest-registration")
    public String guestsRegistration() {
        return "/admin/guest-registration";
    }

    @PostMapping("/guest-registration")
    public String guestsRegistration(@Valid GuestRegistrationDTO data, BindingResult bindingResult,
                                     Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("bindingResult", bindingResult);
        } else {
            if (adminService.registerNewGuest(data, model)) {
                return "redirect:/";
            }
        }
        return "/admin/guest-registration";
    }

    @GetMapping("/guests-replace")
    public String getGuests(
            Model model,
            @RequestParam(defaultValue = "1", required = false) Integer page,
            @RequestParam(defaultValue = "5", required = false) Integer size
    ) {
        var pageable = adminService.getCustomers(page, size, "Guest");
        var guestsList = pageable.stream()
                .map(CustomerDTO::from)
                .collect(Collectors.toList());
        Page<CustomerDTO> guests = new PageImpl<>(guestsList, pageable.getPageable(), pageable.getTotalElements());
        model.addAttribute("guests", guests);
        return "/admin/block-replacement/guests-replace";
    }

    @GetMapping("/employees-replace")
    public String getEmployees(
            Model model,
            @RequestParam(defaultValue = "1", required = false) Integer page,
            @RequestParam(defaultValue = "5", required = false) Integer size
    ) {
        var pageable = adminService.getManagers(page, size, "Manager");
        var employeesList = pageable.stream()
                .map(EmployeeDTO::from)
                .collect(Collectors.toList());
        Page<EmployeeDTO> employees = new PageImpl<>(employeesList, pageable.getPageable(), pageable.getTotalElements());
        model.addAttribute("employees", employees);
        return "/admin/block-replacement/employees-replace";
    }


}
