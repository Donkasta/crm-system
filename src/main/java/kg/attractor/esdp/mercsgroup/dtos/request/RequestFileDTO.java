package kg.attractor.esdp.mercsgroup.dtos.request;


import kg.attractor.esdp.mercsgroup.entities.RequestFile;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequestFileDTO {

    public static RequestFileDTO from(RequestFile requestFile){
        return builder()
                .id(requestFile.getId())
                .requestId(requestFile.getRequest().getId())
                .fileName(requestFile.getFileName())
                .build();

    }

    private Long id;
    private Long requestId;
    private String fileName;

}
