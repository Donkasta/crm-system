package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CustomerServiceTest {

    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    private static final String EMAIL = "cr2@mail.ru";

    @Test
    public void testFindCustomer_CustomerFound(){
        Customer customer = TestUtil.createCustomer(EMAIL);

        when(customerRepository.findByEmail(EMAIL)).thenReturn(Optional.of(customer));

        Customer foundCustomer = customerService.findCustomer(EMAIL);

        verify(customerRepository, times(1)).findByEmail(EMAIL);
        assertEquals(foundCustomer, customer);
    }
}