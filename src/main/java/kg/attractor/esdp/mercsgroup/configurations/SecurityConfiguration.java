package kg.attractor.esdp.mercsgroup.configurations;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import javax.sql.DataSource;

@Configurable
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        JdbcUserDetailsManager userDetailsManager = new JdbcUserDetailsManager();
        userDetailsManager.setDataSource(dataSource);
        userDetailsManager.setUsersByUsernameQuery("select email, password, enabled from customers where email = ?");
        userDetailsManager.setAuthoritiesByUsernameQuery("select c.email, r.role from customers c join roles r on c.role_id = r.id where c.email = ?");
        auth.userDetailsService(userDetailsManager).passwordEncoder(new BCryptPasswordEncoder());

        JdbcUserDetailsManager employeeDetailsManager = new JdbcUserDetailsManager();
        employeeDetailsManager.setDataSource(dataSource);
        employeeDetailsManager.setUsersByUsernameQuery("select email, password, enabled from employees where email = ?");
        employeeDetailsManager.setAuthoritiesByUsernameQuery("select e.email, r.role from employees e join roles r on e.role_id = r.id where e.email = ?");
        auth.userDetailsService(employeeDetailsManager).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/admin/**").hasAnyAuthority("Admin");

        http.authorizeRequests()
                .antMatchers("/profile").authenticated()
                .antMatchers("/employees/**").hasAnyAuthority("Manager")
                .antMatchers("/customers/**").hasAuthority("Customer");

        http.authorizeRequests()
                .antMatchers("/requests/**")
                .hasAnyAuthority("Customer");


        http.authorizeRequests()
                .antMatchers("/orders/status/update")
                .hasAnyAuthority("Customer", "Manager")
                .antMatchers("/orders/**")
                .hasAuthority("Manager");


        http.authorizeRequests()
                .anyRequest()
                .permitAll();

        http.formLogin()
                .loginPage("/login")
                .successHandler((request, response, authentication) -> {
                    response.sendRedirect("/");
                })
                .permitAll();

        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");

        http.csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

}
