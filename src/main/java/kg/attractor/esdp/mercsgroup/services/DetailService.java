package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.entities.Detail;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.repositories.DetailRepository;
import kg.attractor.esdp.mercsgroup.repositories.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DetailService {
    private final DetailRepository detailRepository;
    private final OrderRepository orderRepository;

    private final float PERCENTAGE = 10;
    public Long addNewDetails(DetailDTO detailDTO) {

        Order order = orderRepository.findOrderById(detailDTO.getOrder())
                .orElseThrow(() -> new IllegalArgumentException("Order not found"));

        Detail detail = Detail.builder()
                .customsValue(detailDTO.getCustomsValue())
                .freight(detailDTO.getFreight())
                .productPrice(detailDTO.getProductPrice())
                .productUrl(detailDTO.getProductUrl())
                .quantity(detailDTO.getQuantity())
                .quantityUnit(detailDTO.getQuantityUnit())
                .percentage(PERCENTAGE)
                .finalCost((detailDTO.getFreight() + detailDTO.getProductPrice() * detailDTO.getQuantity()) + (detailDTO.getProductPrice()* detailDTO.getQuantity())/PERCENTAGE)
                .order(order).build();

        detailRepository.save(detail);
        return order.getId();
    }

    public void deleteDetailById(Long id){
        Detail detail = detailRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Detail not found"));
        detailRepository.delete(detail);
    }
}
