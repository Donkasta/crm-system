package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.PasswordRecoveryDTO;
import kg.attractor.esdp.mercsgroup.services.PasswordRecoveryService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class PasswordRecoveryControllerTest {

    @Autowired
    private PasswordRecoveryController passwordRecoveryController;

    @MockBean
    private PasswordRecoveryService passwordRecoveryService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BindingResult bindingResult;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetSpecifyEmailPage(){
        String expectedResponse = "password-recovery-specifying-email";
        String response = passwordRecoveryController.getSpecifyEmailPage();
        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetTokenConfirmationPage() {
        String email = "test@gmail.com";
        String expectedResponse = "password-recovery-token-confirmation";
        when(passwordRecoveryService.sendPasswordRecoveryTokenToEmail(email)).thenReturn(true);
        Model model = new ConcurrentModel();
        String response = passwordRecoveryController.getTokenConfirmationPage(model, email);
        assertEquals(expectedResponse, response);
        verify(passwordRecoveryService, times(1)).sendPasswordRecoveryTokenToEmail(email);
    }

    @Test
    public void testGetTokenConfirmationPage_EmailDoesNotExist() throws Exception{
        String email = " ";
        when(passwordRecoveryService.sendPasswordRecoveryTokenToEmail(any())).thenReturn(false);
        mockMvc.perform(post("/password-recovery/specify-email")
                .param("username", email)
                .with(csrf()))
                .andExpect(status().is3xxRedirection());
        verify(passwordRecoveryService, times(1)).sendPasswordRecoveryTokenToEmail(email);
    }

    @Test
    public void testConfirmToken_SuccessfullyConfirmed() {
        String email = "test@gmail.com";
        String expectedResponse = "password-recovery";
        String token = "token";
        when(passwordRecoveryService.confirmToken(email, token)).thenReturn(true);
        Model model = new ConcurrentModel();
        String response = passwordRecoveryController.confirmToken(model, email, token, null);
        assertEquals(expectedResponse, response);
        verify(passwordRecoveryService, times(1)).confirmToken(email, token);
    }

    @Test
    public void testConfirmToken_NotConfirmed() throws Exception{
        String email = "test@gmail.com";
        String token = "1";
        when(passwordRecoveryService.confirmToken(email, token)).thenReturn(false);
        mockMvc.perform(post("/password-recovery/token-confirmation")
                        .param("username", email)
                        .param("token", token)
                        .with(csrf()))
                .andExpect(status().is3xxRedirection());
        verify(passwordRecoveryService, times(1)).confirmToken(email, token);
    }

    @Test
    public void testRecoverPassword_UnsuccessfullyRecovered(){
        PasswordRecoveryDTO passwordRecoveryDTO = PasswordRecoveryDTO.builder()
                .email("test@gmail.com")
                .password("password")
                .confirmPassword("password")
                .build();
        Model model = new ConcurrentModel();
        String expectedResponse = "password-recovery";
        when(bindingResult.hasErrors()).thenReturn(true);
        String response = passwordRecoveryController.recoverPassword(passwordRecoveryDTO, bindingResult, model, null);

        assertEquals(expectedResponse, response);
        verify(passwordRecoveryService, never()).recoverPassword(any());
    }

    @Test
    public void testRecoverPassword_SuccessfullyRecovered() throws Exception{
        PasswordRecoveryDTO passwordRecoveryDTO = TestUtil.createPasswordRecoveryDto();
        mockMvc.perform(post("/password-recovery")
                .param("email", passwordRecoveryDTO.getEmail())
                .param("password", passwordRecoveryDTO.getPassword())
                .param("confirmPassword", passwordRecoveryDTO.getConfirmPassword())
                .with(csrf()))
                .andExpect(status().is3xxRedirection());

        verify(passwordRecoveryService, times(1)).recoverPassword(any());
    }
}