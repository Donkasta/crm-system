package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.RegistrationDTO;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.RoleRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class RegistrationServiceTest {

    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private EmployeeRepository employeeRepository;
    @MockBean
    private RoleRepository roleRepository;
    @Autowired
    private RegistrationService registrationService;

    @Test
    public void testRegisterNewCustomer_EmployeeFoundBehavior() {
        RegistrationDTO registrationDTO = TestUtil.createRegistrationDto();
        Model model = new ConcurrentModel();
        Employee employee = TestUtil.createEmployee();
        when(employeeRepository.findByEmail(registrationDTO.getEmail())).thenReturn(Optional.of(employee));
        boolean response = registrationService.registerNewCustomer(registrationDTO, model);
        assertFalse(response);
        String expectedModelResponse = "Данный email уже зарегистрирован как Сотрудник!";
        assertEquals(expectedModelResponse, model.getAttribute("message"));
    }

    @Test
    public void testRegisterNewCustomer_CustomerFoundBehavior() {
        RegistrationDTO registrationDTO = TestUtil.createRegistrationDto();
        Model model = new ConcurrentModel();
        when(employeeRepository.findByEmail(registrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(registrationDTO.getEmail()))
                .thenReturn(Optional.of(TestUtil.createCustomer("cr1@mail.ru")));
        boolean response = registrationService.registerNewCustomer(registrationDTO, model);
        assertFalse(response);
        String expectedModelResponse = "Данный email уже зарегистрирован!";
        assertEquals(expectedModelResponse, model.getAttribute("message"));
    }

    @Test
    public void testRegisterNewCustomer_UniqueEmailRegistration() {
        RegistrationDTO registrationDTO = TestUtil.createRegistrationDto();
        Model model = new ConcurrentModel();
        Role role = TestUtil.createRole();
        when(employeeRepository.findByEmail(registrationDTO.getEmail())).thenReturn(Optional.empty());
        when(customerRepository.findByEmail(registrationDTO.getEmail())).thenReturn(Optional.empty());
        when(roleRepository.findById(2L)).thenReturn(Optional.of(role));
        boolean response = registrationService.registerNewCustomer(registrationDTO, model);
        assertTrue(response);
        verify(customerRepository, times(1)).save(any());
    }
}