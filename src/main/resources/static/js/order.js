function updateStatus (orderId) {
    let csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
    let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");
    let status = document.getElementById("status").value;

    const data = {
        orderId: orderId,
        status: status
    };

    fetch('/orders/status/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            [csrfHeader]: csrfToken
        },
        body: JSON.stringify(data),
    })
        .then(response => {
            if (response.ok) {
                window.location.reload();
            } else {
                console.error('Error:', response.statusText);
            }
        })
}

function deleteDetail(detailId) {
    let csrfToken = document.querySelector("meta[name='_csrf']").getAttribute("content");
    let csrfHeader = document.querySelector("meta[name='_csrf_header']").getAttribute("content");

    fetch(`/orders/details/delete?detail_id=${detailId}`, {
        method: 'POST',
        headers: {
            [csrfHeader]: csrfToken
        }
    })
        .then(response => {
            if (response.ok) {
                window.location.reload()
            } else {
                console.error('Error:', response.statusText);
            }
        })
}