alter table customers
    add activity_id bigint references activities on delete cascade on update cascade,
    add orders_quantity integer default 0