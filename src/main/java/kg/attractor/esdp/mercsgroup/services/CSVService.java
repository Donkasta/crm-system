package kg.attractor.esdp.mercsgroup.services;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import kg.attractor.esdp.mercsgroup.entities.Activity;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.repositories.ActivityRepository;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class CSVService {
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;
    private final ActivityRepository activityRepository;

    public boolean uploadCSVFile(MultipartFile file) throws IOException, CsvException, DataIntegrityViolationException {
        if (!Objects.equals(file.getContentType(), MediaType.TEXT_PLAIN_VALUE) && !Objects.equals(file.getContentType(),
                "text/csv")) {
            return false;
        }
        try (Reader reader = new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8);
             CSVReader csvReader = new CSVReader(reader)) {

            String[] headers = csvReader.readNext();

            List<String> headerList = Arrays.asList(headers);

            int firstNameIndex = headerList.indexOf("First Name");
            int emailIndex = headerList.indexOf("Email");
            int numberIndex = headerList.indexOf("First Phone");
            int companyIndex = headerList.indexOf("Company");
            int postIndex = headerList.indexOf("Seniority");
            int industryIndex = headerList.indexOf("Industry");

            List<Customer> customerList = new ArrayList<>();
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                if (line.length > firstNameIndex && firstNameIndex != -1) {
                    String firstName = line[firstNameIndex];

                    Customer customer = Customer.builder()
                            .name(firstName)
                            .customerData(LocalDateTime.now().toString())
                            .build();

                    Optional<Role> role = roleRepository.findByRole("Guest");
                    role.ifPresent(customer::setRole);

                    if (line.length > emailIndex && !line[emailIndex].isEmpty()) {
                        String email = line[emailIndex];
                        customer.setEmail(email);
                    }

                    if (line.length > numberIndex && !line[numberIndex].isEmpty()) {
                        String number = line[numberIndex];
                        customer.setNumber(number);
                    }

                    if (line.length > companyIndex && !line[companyIndex].isEmpty()) {
                        String company = line[companyIndex];
                        customer.setCompany(company);
                    }

                    if (line.length > postIndex && !line[postIndex].isEmpty()) {
                        String post = line[postIndex];
                        customer.setPost(post);
                    }

                    if (line.length > industryIndex && !line[industryIndex].isEmpty()) {
                        setCustomerIndustry(customer, line[industryIndex]);
                    }
                    customer.setOrdersQuantity(0);
                    customerList.add(customer);
                }
            }
            try {
                saveAllOrUpdate(customerList);
                return true;
            } catch (DataIntegrityViolationException ex) {
                return false;
            }
        } catch (IOException e) {
            throw new IOException();
        } catch (CsvException ex){
            throw new CsvException();
        }
    }

    private void setCustomerIndustry(Customer customer, String industry){
        Optional<Activity> optionalActivity = activityRepository.findByActivity(industry);
        if(optionalActivity.isPresent()){
            customer.setActivity(optionalActivity.get());
        } else {
            Activity activity = new Activity();
            activity.setActivity(industry);
            activityRepository.save(activity);
            customer.setActivity(activity);
        }
    }

    private void saveAllOrUpdate(List<Customer> customers) {
        for (Customer customer : customers) {
            if (((customer.getId()!=null && customerRepository.existsById(customer.getId())) || customerRepository.existsByEmail(customer.getEmail())
                    || customerRepository.existsByNumber(customer.getNumber()))) {
                customerRepository.updateCustomerFieldsById(customer.getName(), customer.getEmail(), customer.getRole(),
                        customer.getActivity(), customer.getPost(), customer.getCompany(), customer.getId());
            } else {
                customerRepository.save(customer);
            }
        }
        customerRepository.flush();
    }
}