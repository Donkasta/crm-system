package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {

    Page<Request> findAll(Pageable pageable);

    Optional<Request> findById(Long id);

    Page<Request> findRequestsByCustomerId(Long customerId, Pageable pageable);

    @Query("select r from Request r where r.customer.email = :email")
    Page<Request> findByCustomerEmail(@Param("email") String email, Pageable pageable);

    @Query("select r from Request r where r.status.status = :status order by r.requestDate desc")
    Page<Request> findAllByStatus(@Param("status") String status, Pageable pageable);

    Page<Request> findAllByProduct(String product, Pageable pageable);

    @Query("select r from Request r order by r.requestDate desc")
    Page<Request> findAllByPage(Pageable pageable);

    boolean existsByIdAndCustomerEmail(Long id, String email);
}
