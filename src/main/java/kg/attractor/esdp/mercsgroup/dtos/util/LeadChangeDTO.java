package kg.attractor.esdp.mercsgroup.dtos.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class LeadChangeDTO {

    @NotNull
    @Min(value = 1, message = "Ошибка в скрытом поле Id заказа")
    private Long orderId;

    @Pattern(regexp = "Холодный|Теплый|Горячий", flags = Pattern.Flag.CASE_INSENSITIVE,message = "Такого лида не существует")
    private String lead;

    @FutureOrPresent(message = "Дата окончания лида не должна быть в прошлом")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime endDate;



}
