package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Request;
import kg.attractor.esdp.mercsgroup.entities.RequestFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RequestFileRepository extends JpaRepository<RequestFile, Long> {

    Optional<RequestFile> findByRequest(Request request);

    Boolean existsByFileName(String fileName);

    @Query("select r from RequestFile r where r.request.id = :id")
    Optional<RequestFile> findByRequestId(Long id);

    RequestFile findByFileName(String name);
}
