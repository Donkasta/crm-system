package kg.attractor.esdp.mercsgroup.dtos.authorization;

import kg.attractor.esdp.mercsgroup.validators.UniquePhone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
@Builder
public class RegistrationDTO {
    @NotBlank(message = "Введите ваш email")
    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 16, message = "Пароль должен содержать минимум 8 символов")
    private String password;

    @NotBlank(message = "Введите ваше имя")
    private String name;
    private String company;

    @UniquePhone
    @NotBlank(message = "Введите номер телефона")
    @Pattern(regexp = "^(\\+996\\s?)?(\\d{3}\\s?)?\\d{6}$", message = "Введите номер телефона в формате: +996000000000")
    private String phone;

    private String csrfToken;
}
