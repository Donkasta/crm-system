package kg.attractor.esdp.mercsgroup.dtos.detail;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetailWithIdDTO {

    private Long id;
    private String productUrl;
    private Double quantity;
    private Double price;
    private Double freight;
    private Double customsValue;
}
