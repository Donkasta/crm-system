create table if not exists csrf
(
    id                   bigserial
        primary key,
    csrf_token           varchar,
    created_date_time    timestamp,
    expiration_date_time timestamp,
    employee_id          bigint
        references employees (id)
);