package kg.attractor.esdp.mercsgroup.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class NewRequestDTO {
    private Long customerId;
    @NotBlank(message = "Введите название")
    private String product;
    @NotBlank(message = "Введите описание")
    private String description;
    @NotBlank(message = "Введите количество")
    private String quantity;
    private String information;

    @Size(max = 10, message = "Количество файлов должно быть до 10")
    private MultipartFile[] files;

    @NotNull(message = "Выберите тип доставки")
    private Long deliveryId;
}
