package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderEditDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.LeadChangeDTO;
import kg.attractor.esdp.mercsgroup.entities.Order;
import kg.attractor.esdp.mercsgroup.services.DetailService;
import kg.attractor.esdp.mercsgroup.services.EmployeeService;
import kg.attractor.esdp.mercsgroup.services.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;
    private final DetailService detailService;

    private final EmployeeService employeeService;


    @PostMapping()
    public String newOrder(@RequestParam("request_id") Long id,
                           Authentication authentication) {
        String email = authentication.getName();
        Long orderId = orderService.addNewOrder(id, email).getId();
        return "redirect:/orders?order_id=" + orderId;
    }

    @GetMapping()
    public String getOrder(@RequestParam("order_id") Long id,
                           Model model,Authentication authentication) {
        var manager = employeeService.findEmployee(authentication.getName());
        OrderDTO order = OrderDTO.from(orderService.getManagerOrder(id,manager.getId()));
        model.addAttribute("order", order);
        model.addAttribute("currentDate", Timestamp.valueOf(LocalDateTime.now()));
        return "order";
    }

    @PostMapping("/deleteLead")
    public String deleteLead(Long orderId){
        orderService.deleteLead(orderId);
        return "redirect:/orders?order_id="+orderId;
    }

    @PostMapping("/changeLead")
    public String leadChange(@Valid LeadChangeDTO lead, BindingResult bindingResult,
                             Model model){
            if(bindingResult.hasErrors()){
                model.addAttribute("bindingResult", bindingResult);
                OrderDTO order = OrderDTO.from(orderService.getOrderById(lead.getOrderId()));
                model.addAttribute("order", order);
                model.addAttribute("currentDate", Timestamp.valueOf(LocalDateTime.now()));
                return "order";
            }else{
                orderService.changeLead(lead);
            }
        return "redirect:/orders?order_id="+lead.getOrderId();
    }

    @PostMapping("/delete")
    public String deleteOrder(@RequestParam("order") Long id) {
        orderService.deleteOrder(id);
        return "redirect:/";
    }

    @PostMapping("/cancel")
    public String cancelOrder(@RequestParam("order") Long id) {
        orderService.cancelOrder(id);
        return "redirect:/";
    }

    @PostMapping("/changeDetails")
    public String addDetails(@RequestParam(value = "detail_id", defaultValue = "0") Long detail_id,
                             @Valid DetailDTO detailDTO, BindingResult bindingResult,
                             RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
            return "redirect:/orders?order_id=" + detailDTO.getOrder();
        }
        if (detail_id == 0) {
            detailService.addNewDetails(detailDTO);
        } else {
            orderService.saveDetailsToOrder(detail_id, detailDTO);
        }

        return "redirect:/orders?order_id=" + detailDTO.getOrder();
    }


    @PostMapping("/change")
    public ResponseEntity<Object> changeOrderDetails(@ModelAttribute @Valid OrderEditDTO orderDTO,
                                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<Map<String, String>> errors = new ArrayList<>();
            for (FieldError error : bindingResult.getFieldErrors()) {
                Map<String, String> errorMap = new HashMap<>();
                errorMap.put("field", error.getField());
                errorMap.put("message", error.getDefaultMessage());
                errors.add(errorMap);
            }
            return ResponseEntity.badRequest().body(errors);
        }

        if (orderService.editOrder(orderDTO)) {
            return ResponseEntity.ok("ok");
        } else {
            return ResponseEntity.badRequest().body("Неизвестная ошибка");
        }
    }


    @PostMapping("/confirm")
    public String confirmOrder(@RequestParam("orderId") Long orderId,
                               @RequestParam("status") String status,
                               RedirectAttributes redirectAttributes) {
        Order order = orderService.getOrderById(orderId);
        orderService.confirmOrder(order, status);
        redirectAttributes.addFlashAttribute("statusMessage", "Статус заказа изменен");
        return "redirect:/orders/createOffer?order_id=" + orderId;
    }

    @PostMapping("/status/update")
    public String updateStatus(@RequestBody Map<String, Object> data) {
        Long orderId = Long.valueOf(data.get("orderId").toString());
        String status = data.get("status") != null ? data.get("status").toString() : null;

        Order order = orderService.getOrderById(orderId);
        orderService.updateOrderStatus(order, status);

        if (order.getStatus().getStatus().equals("Ожидается подтверждение")) {
            return "redirect:/orders/createOffer?order_id=" + orderId;
        }

        return "redirect:/";
    }

    @GetMapping("/createOffer")
    public String createOffer(@RequestParam("order_id") Long id) {
        String email = orderService.getOrderById(id).getCustomer().getEmail();
        orderService.sendPdfOffer(email, id);
        return "redirect:/";
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("order_id") Long id) {
        String filePath = orderService.convertPdfOffer(id);
        Resource fileResource = new FileSystemResource(filePath);

        if (fileResource.exists()) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileResource.getFilename());

            String contentType = URLConnection.guessContentTypeFromName(fileResource.getFilename());
            headers.setContentType(MediaType.parseMediaType(contentType));
            return ResponseEntity.ok()
                    .headers(headers)
                    .body(fileResource);
        } else {
            System.out.println("Не удалось загрузить файл");
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/details/delete")
    public ResponseEntity<String> deleteOrderDetail(@RequestParam("detail_id") Long id) {
        detailService.deleteDetailById(id);
        return ResponseEntity.ok("Успешно удалено");
    }
}