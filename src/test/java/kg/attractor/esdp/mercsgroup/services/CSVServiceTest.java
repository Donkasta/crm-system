package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Activity;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.repositories.ActivityRepository;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
class CSVServiceTest {

    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private ActivityRepository activityRepository;

    @Autowired
    private CSVService csvService;

    @Test
    public void testUploadCSVFile_SaveDataToDatabase_Successful() throws Exception {
        MockMultipartFile file = TestUtil.createMockMultipartFile("test.csv", "text/csv");

        boolean bool = csvService.uploadCSVFile(file);
        assertTrue(bool);
    }

    @Test
    public void testUploadCSVFile_SaveDataToDatabase_Unsuccessful() throws Exception {
        MockMultipartFile file = TestUtil.createMockMultipartFile("test.pdf", "application/pdf");

        boolean bool = csvService.uploadCSVFile(file);
        assertFalse(bool);
    }

    @Test
    public void testSetCustomerIndustry_ExistingActivity() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setCustomerIndustryMethod = CSVService.class.getDeclaredMethod("setCustomerIndustry", Customer.class, String.class);
        setCustomerIndustryMethod.setAccessible(true);

        String activityName = "HoReCa";
        Activity activity = TestUtil.createActivity();
        when(activityRepository.findByActivity(activityName)).thenReturn(Optional.of(activity));
        setCustomerIndustryMethod.invoke(csvService,
                TestUtil.createCustomer("test@gmail.com"), activityName);
        verify(activityRepository, never()).save(any());
        setCustomerIndustryMethod.setAccessible(false);
    }

    @Test
    public void testSetCustomerIndustry_NotExistingActivity() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setCustomerIndustryMethod = CSVService.class.getDeclaredMethod("setCustomerIndustry", Customer.class, String.class);
        setCustomerIndustryMethod.setAccessible(true);

        String activityName = "HoReCa";
        when(activityRepository.findByActivity(activityName)).thenReturn(Optional.empty());
        setCustomerIndustryMethod.invoke(csvService,
                TestUtil.createCustomer("test@gmail.com"), activityName);
        verify(activityRepository, times(1)).save(any());
        setCustomerIndustryMethod.setAccessible(false);
    }

    @Test
    public void testSaveAllOrUpdate_SavesNewCustomers_UpdatesExistingCustomers() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setCustomerIndustryMethod = CSVService.class.getDeclaredMethod("saveAllOrUpdate", List.class);
        setCustomerIndustryMethod.setAccessible(true);

        String email = "test@gmail.com";
        when(customerRepository.existsByEmail(email)).thenReturn(true);
        List<Customer> customerList = Collections.singletonList(TestUtil.createCustomer(email));
        setCustomerIndustryMethod.invoke(csvService,
                customerList);
        verify(customerRepository, never()).save(any());
        setCustomerIndustryMethod.setAccessible(false);
    }

    @Test
    public void testSaveAllOrUpdate_SavesNewCustomers() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setCustomerIndustryMethod = CSVService.class.getDeclaredMethod("saveAllOrUpdate", List.class);
        setCustomerIndustryMethod.setAccessible(true);

        String email = "test@gmail.com";
        List<Customer> customerList = Collections.singletonList(TestUtil.createCustomer(email));
        setCustomerIndustryMethod.invoke(csvService,
                customerList);
        verify(customerRepository, times(1)).save(customerList.get(0));
        setCustomerIndustryMethod.setAccessible(false);
    }
}