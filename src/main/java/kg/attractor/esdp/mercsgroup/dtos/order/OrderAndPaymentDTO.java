package kg.attractor.esdp.mercsgroup.dtos.order;


import kg.attractor.esdp.mercsgroup.dtos.customer.CustomerDTO;
import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.employee.EmployeeDTO;
import kg.attractor.esdp.mercsgroup.dtos.request.RequestDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.StatusDTO;
import kg.attractor.esdp.mercsgroup.entities.Detail;
import kg.attractor.esdp.mercsgroup.entities.Order;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class OrderAndPaymentDTO {
    public static OrderAndPaymentDTO from(Order order, List<Detail> details) {
        return OrderAndPaymentDTO.builder()
                .id(order.getId())
                .customer(CustomerDTO.from(order.getCustomer()))
                .employee(EmployeeDTO.from(order.getEmployee()))
                .orderDate(Timestamp.valueOf(order.getOrderDate()))
                .request(RequestDTO.from(order.getRequest()))
                .status(StatusDTO.from(order.getStatus()))
                .details(order.getDetails().stream().map(DetailDTO::from).collect(Collectors.toList()))
                .productName(order.getRequest().getProduct())
                .productPrice((float) details.stream().mapToDouble(Detail::getProductPrice).sum())
                .productQty((int) details.stream().mapToDouble(Detail::getQuantity).sum())
                .deliveryPrice((float) details.stream().mapToDouble(Detail::getFreight).sum())
                .servicePrice((float) details.stream()
                        .mapToDouble(detail -> detail.getProductPrice() / detail.getPercentage())
                        .sum())
                .build();
    }


    private Long id;
    private CustomerDTO customer;
    private EmployeeDTO employee;
    private Timestamp orderDate;
    private RequestDTO request;
    private StatusDTO status;
    private List<DetailDTO> details;
    private String productName;
    private float productPrice;
    private int productQty;
    private float deliveryPrice;
    private float servicePrice;
}
