package kg.attractor.esdp.mercsgroup.services;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderAndPaymentDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderEditDTO;
import kg.attractor.esdp.mercsgroup.dtos.util.LeadChangeDTO;
import kg.attractor.esdp.mercsgroup.entities.*;
import kg.attractor.esdp.mercsgroup.repositories.*;
import kg.attractor.esdp.mercsgroup.utils.EmailUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final EmployeeRepository employeeRepository;
    private final DetailRepository detailRepository;
    private final StatusRepository statusRepository;
    private final RequestRepository requestRepository;

    public Order addNewOrder(Long id, String email) {

        Request request = requestRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Request not found"));

        Customer customer = customerRepository.findById(request.getCustomer().getId())
                .orElseThrow(() -> new IllegalArgumentException("Customer not found"));

        Employee employee = employeeRepository.findByEmail(email)
                .orElseThrow(() -> new IllegalArgumentException("Employee not found"));

        Status status = statusRepository.findStatusByStatus("В обработке")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        request.setStatus(status);

        Order order = Order.builder()
                .orderDate(LocalDateTime.now())
                .customer(customer)
                .employee(employee)
                .request(request)
                .status(status).build();

        orderRepository.save(order);
        requestRepository.save(request);

        updateOrdersQuantity(customer);

        return order;
    }

    private void updateOrdersQuantity(Customer customer) {
        int orderQuantity = orderRepository.countAllByCustomerId(customer.getId());
        customer.setOrdersQuantity(orderQuantity);
        customerRepository.save(customer);
    }

    public Page<Order> getCustomerOrders(Long customerId, Integer page, Integer size,
                                         String sortBy, int sortOrder, Optional<String> productName, Optional<String> description, Optional<String> status) {
        Pageable pageable;
        page = getPage(page);

        if (sortOrder == 1) {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));
        }

        return orderRepository.searchByProductAndDescriptionAndStatus(productName, description, status, customerId, pageable);
    }

    public Page<Order> searchCustomerOrders(Long customerId, Integer page, Integer size, Optional<String> productName,
                                            Optional<String> description, Optional<String> status) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return orderRepository.searchByProductAndDescriptionAndStatus(productName, description, status, customerId, pageable);
    }


    private Integer getPage(Integer page) {
        if (page != null && page > 0) {
            return page - 1;
        }
        return 0;
    }

    public Order getOrderById(Long id) {
        return orderRepository.findOrderById(id).orElseThrow(() -> new IllegalArgumentException("Order not found"));
    }

    public void deleteOrder(Long id) {
        Order order = getOrderById(id);
        Request request = order.getRequest();

        orderRepository.delete(order);
        requestRepository.delete(request);
    }

    public void saveDetailsToOrder(Long detail_id, DetailDTO detailDTO) {

        Optional<Detail> detailOptional = detailRepository.findById(detail_id);

        if (detailOptional.isEmpty()) {

            detailRepository.save(Detail.builder()
                    .order(orderRepository.findOrderById(detailDTO.getOrder()).orElseThrow())
                    .productPrice(detailDTO.getProductPrice())
                    .freight(detailDTO.getFreight())
                    .quantity(detailDTO.getQuantity())
                    .quantityUnit(detailDTO.getQuantityUnit())
                    .customsValue(detailDTO.getCustomsValue())
                    .productUrl(detailDTO.getProductUrl())
                    .build());

        } else {

            Detail detail = detailOptional.get();

            detail.setProductPrice(detailDTO.getProductPrice());
            detail.setFreight(detailDTO.getFreight());
            detail.setQuantity(detailDTO.getQuantity());
            detail.setQuantityUnit(detailDTO.getQuantityUnit());
            detail.setCustomsValue(detailDTO.getCustomsValue());
            detail.setProductUrl(detailDTO.getProductUrl());

            detailRepository.save(detail);
        }
    }


    public Page<Order> getOrdersByEmployee(String email, Integer page, Integer size, String sortBy, int sortOrder) {
        page = getPage(page);
        Pageable pageable;

        if (sortOrder == 1) {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));
        }
        return orderRepository.findAllByEmployeesEmail(email, pageable);
    }

    public boolean editOrder(OrderEditDTO orderDTO) {
        Order order = orderRepository.findOrderById(orderDTO.getId())
                .orElseThrow(() -> new IllegalArgumentException("Order not found"));

        Status status = statusRepository.findStatusByStatus("В обработке")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        for (DetailDTO dto : orderDTO.getDetails()) {
            Detail detail = detailRepository.findById(dto.getId())
                    .orElseThrow(() -> new IllegalArgumentException("Details not found"));

            if (dto.getProductUrl().isEmpty() || dto.getQuantity() <= 0 || dto.getQuantityUnit().isEmpty()) {
                return false;
            } else {
                detail.setProductUrl(dto.getProductUrl());
                detail.setQuantity(dto.getQuantity());
                detail.setQuantityUnit(dto.getQuantityUnit());
            }

            try {
                detail.setQuantity(dto.getQuantity());
                detail.setProductPrice(dto.getProductPrice());
                detail.setFreight(dto.getFreight());
                detail.setCustomsValue(dto.getCustomsValue());
                if (dto.getProductPrice() < 0 || dto.getFreight() < 0 || dto.getCustomsValue() < 0) {
                    return false;
                }
            } catch (NullPointerException e) {
                return false;
            }
            detailRepository.save(detail);
        }
        order.setStatus(status);
        orderRepository.save(order);

        return true;
    }


    public Page<OrderAndPaymentDTO> getEmployeePayments(String email,
                                                        Integer page,
                                                        Integer size,
                                                        String sortBy,
                                                        int sortOrder) {
        Pageable pageable;
        if (sortOrder == 1) {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));
        }

        Page<Order> orders = orderRepository.findAllByEmployeeEmailAndStatusIdGreaterThan(email,
                4L, pageable);
        List<OrderAndPaymentDTO> dtoList = getDtoList(orders.getContent());
        return new PageImpl<>(dtoList, pageable, orders.getTotalElements());
    }

    public Page<OrderAndPaymentDTO> getCustomerPayments(String email, Integer page, Integer size) {
        Pageable pageable = PageRequest.of(getPage(page),size);
        Page<Order> orders = orderRepository.findAllByCustomerEmailAndStatusIdGreaterThan(email,4L, getPageableObject(page, size));
        List<OrderAndPaymentDTO> dtoList = getDtoList(orders.getContent());
        return new PageImpl<>(dtoList, pageable, orders.getTotalElements());
    }

    private List<OrderAndPaymentDTO> getDtoList(List<Order> orders) {
        return orders.stream()
                .map(e -> OrderAndPaymentDTO.from(e, e.getDetails()))
                .collect(Collectors.toList());
    }

    private Pageable getPageableObject(Integer page, Integer size) {
        page = getPage(page);
        return PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "orderDate")); // sortBy = "orderDate";
    }

    public void confirmOrder(Order order, String status) {

        Status statusWaitingForConfirmation = statusRepository.findStatusByStatus("Ожидается подтверждение")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        Status statusWaitingForPayment = statusRepository.findStatusByStatus("Ожидается оплата")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        Status statusCompleted = statusRepository.findStatusByStatus("Завершен")
                .orElseThrow(() -> new IllegalArgumentException("Status not found"));

        Request request = requestRepository.findById(order.getRequest().getId())
                .orElseThrow(() -> new IllegalArgumentException("Request not found"));


        switch (status) {
            case "Ожидается подтверждение":
                order.setStatus(statusWaitingForConfirmation);
                request.setStatus(statusCompleted);
                requestRepository.save(request);
                break;
            case "Ожидается оплата":
                order.setStatus(statusWaitingForPayment);
                request.setStatus(statusCompleted);
                requestRepository.save(request);
                break;
        }
        orderRepository.save(order);
    }

    public void updateOrderStatus(Order order, String statusName) {

        Request request = requestRepository.findById(order.getRequest().getId())
                .orElseThrow(() -> new IllegalArgumentException("Request not found"));

        if (!"Завершен".equals(request.getStatus().getStatus())) {
            Status completedStatus = statusRepository.findStatusByStatus("Завершен")
                    .orElseThrow(() -> new IllegalArgumentException("Status 'Завершен' not found"));
            request.setStatus(completedStatus);
            requestRepository.save(request);
        }

        if (StringUtils.isNotEmpty(statusName)) {
            Status status = statusRepository.findStatusByStatus(statusName)
                    .orElseThrow(() -> new IllegalArgumentException("Status not found"));
            order.setStatus(status);
        } else {
            String currentStatus = order.getStatus().getStatus();
            Status status = statusRepository.findStatusByStatus(currentStatus)
                    .orElseThrow(() -> new IllegalArgumentException("Status not found"));
            switch (currentStatus) {
                case "Новый":
                    status = statusRepository.findStatusByStatus("В обработке")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
                case "В обработке":
                    status = statusRepository.findStatusByStatus("Ожидается подтверждение")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
                case "Ожидается подтверждение":
                    status = statusRepository.findStatusByStatus("Ожидается оплата")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
                case "Ожидается оплата":
                    status = statusRepository.findStatusByStatus("В процессе")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
                case "В процессе":
                    status = statusRepository.findStatusByStatus("Отправлен")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
                case "Отправлен":
                    status = statusRepository.findStatusByStatus("Завершен")
                            .orElseThrow(() -> new IllegalArgumentException("Status not found"));
                    break;
            }
            order.setStatus(status);
        }
        orderRepository.save(order);
    }

    public void sendPdfOffer(String email, Long orderId) {
        String filePath = convertPdfOffer(orderId);
        if (filePath != null) {
            EmailUtil.sendOfferToClient(email, filePath);
        }
    }

    public String convertPdfOffer(Long orderId) {
        String inputPath = "storage/offer/offer_for_client.html";
        String outputPath = "storage/offer/offer_template.pdf";

        try {
            resetHtmlOffer();
            editOffer(orderId);
            String htmlContent = new String(Files.readAllBytes(Paths.get(inputPath)));
            convertHtmlToPdf(htmlContent, outputPath);
            clearPage();
            return outputPath;
        } catch (IOException e) {
            System.out.println("Ошибка при чтении файла: " + e.getMessage());
            return null;
        }
    }

    public static void convertHtmlToPdf(String htmlContent, String outputPath) {
        try {
            ConverterProperties converterProperties = new ConverterProperties();
            HtmlConverter.convertToPdf(htmlContent, new FileOutputStream(outputPath), converterProperties);
            System.out.println("PDF файл успешно создан: " + outputPath);
        } catch (Exception e) {
            System.out.println("Ошибка при создании PDF файла: " + e.getMessage());
        }
    }

    public void editOffer(Long orderId) {
        Order order = orderRepository.findOrderById(orderId).orElseThrow();

        //Комиссионные проценты за услуги
        int commission = 10;

        try {
            Document doc = Jsoup.parse(new File("storage/offer/offer_for_client.html"), "UTF-8");

            Element h2Element = doc.selectFirst("h2");
            if (h2Element != null) {
                h2Element.text("Название товара: " + order.getRequest().getProduct());
            }

            Element linkElement = doc.selectFirst("a.link");
            if (linkElement != null) {
                linkElement.attr("href", order.getDetails().get(0).getProductUrl()).text("Перейти по ссылке");
            }

            Element qtyElement = doc.selectFirst("p.qty");
            if (qtyElement != null) {
                qtyElement.text("Количество: " + order.getDetails().get(0).getQuantity());
            }

            Element priceElement = doc.selectFirst("p.price");
            if (priceElement != null) {
                priceElement.text("Цена товара: " + order.getDetails().get(0).getProductPrice());
            }

            double totalPrice = order.getDetails().get(0).getProductPrice() *
                    order.getDetails().get(0).getQuantity();

            Element totalPriceElement = doc.selectFirst("p.total");
            if (totalPriceElement != null) {
                totalPriceElement.text("Сумма товаров: " + totalPrice);
            }

            double freightFirst = order.getDetails().get(0).getFreight();

            Element freightElement = doc.selectFirst("p.freight");
            if (freightElement != null) {
                freightElement.text("Фрахт: " + freightFirst);
            }

            double customValueFirst = order.getDetails().get(0).getCustomsValue();
            Element customValueElement = doc.selectFirst("p.custom_value");
            if (customValueElement != null) {
                customValueElement.text("Таможенная стоимость: " + customValueFirst);
            }
            double totalSum;

            if (order.getDetails().size() > 1) {

                for (int i = 1; i < order.getDetails().size(); i++) {
                    Element divElement = doc.selectFirst("div.chapters");
                    Element chapterDivElement = doc.createElement("div").
                            addClass("chapter");
                    if (divElement != null) {
                        divElement.appendChild(chapterDivElement);
                    }

                    Element newLinkElement = doc.
                            createElement("a").
                            addClass("link").
                            attr("href", order.getDetails().get(i).getProductUrl()).text("Ссылка на товар: ");
                    Element linkText = doc.createElement("u").text("Перейти по ссылке");
                    newLinkElement.appendChild(linkText);

                    Element newTitleElement = doc.createElement("h4")
                            .addClass("chapterTitle")
                            .text("Часть " + (i + 1));

                    Element newQtyElement = doc.createElement("p").
                            text("Количество: " + order.getDetails().get(i).getQuantity());

                    Element newPriceElement = doc.createElement("p").
                            text("Цена товара: " + order.getDetails().get(i).getProductPrice());

                    Element newTotalPriceElement = doc.createElement("p").
                            addClass("total").
                            text("Сумма товаров: " + (
                                    order.getDetails().get(i).getProductPrice() *
                                            order.getDetails().get(i).getQuantity()));

                    Element newFreightElement = doc.createElement("p")
                            .addClass("freight")
                            .text("Фрахт: " + order.getDetails().get(i).getFreight());

                    Element newCustomValueElement = doc.createElement("p")
                            .addClass("custom_value")
                            .text("Таможенная стоимость: " + order.getDetails().get(i).getCustomsValue());

                    chapterDivElement.appendChild(newTitleElement);
                    chapterDivElement.appendChild(newLinkElement);
                    chapterDivElement.appendChild(newQtyElement);
                    chapterDivElement.appendChild(newPriceElement);
                    chapterDivElement.appendChild(newTotalPriceElement);
                    chapterDivElement.appendChild(newFreightElement);
                    chapterDivElement.appendChild(newCustomValueElement);

                    totalPrice += order.getDetails().get(i).getProductPrice() *
                            order.getDetails().get(i).getQuantity();
                    freightFirst += order.getDetails().get(i).getFreight();
                    customValueFirst += order.getDetails().get(i).getCustomsValue();


                }
            }

            double percent = (totalPrice + freightFirst + customValueFirst) / 100 * commission;
            totalSum = totalPrice + freightFirst + customValueFirst + percent;


            Element totalSumElement = doc.selectFirst("b.total_sum");
            if (totalSumElement != null) {
                totalSumElement.text("Итоговая стоимость: " + totalSum);
            }

            File outputFile = new File("storage/offer/offer_for_client.html");
            FileWriter fileWriter = new FileWriter(outputFile);
            fileWriter.write(doc.outerHtml());
            fileWriter.close();
        } catch (
                IOException e) {
            e.printStackTrace();
        }

    }

    private void resetHtmlOffer() {
        replaceHtmlPage("<!doctype html>\n" +
                "<html lang=\"ru\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\"\n" +
                "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                "    <title>Коммерческое предложение</title>\n" +
                "    <style>\n" +
                "        body {\n" +
                "            font-family: Arial, sans-serif;\n" +
                "            color: #000;\n" +
                "            background-color: #fff;\n" +
                "            margin: 20px;\n" +
                "            padding: 20px;\n" +
                "        }\n" +
                "\n" +
                "        h1, h2 {\n" +
                "            color: #000;\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "\n" +
                "        p {\n" +
                "            color: #000;\n" +
                "            margin-bottom: 10px;\n" +
                "        }\n" +
                "\n" +
                "        a {\n" +
                "            color: #000;\n" +
                "            text-decoration: none;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Коммерческое предложение</h1>\n" +
                "<p>Мы благодарны за вашу заявку и готовы предложить вам следующий товар:</p>\n" +
                "<h2>Название товара: игровой ноутбук</h2>\n" +
                "<br>\n" +
                "<p>Ссылка на товар: <u><a class=\"link\"></a></u></p>\n" +
                "<p class=\"qty\"></p>\n" +
                "<p class=\"price\"></p>\n" +
                "<p class=\"total\"></p>\n" +
                "<p class=\"freight\"></p>\n" +
                "<p class=\"custom_value\"></p>\n" +
                "<br>\n" +
                "<div class=\"chapters\"></div>\n" +
                "  <p>Комиссия компании: 10%</p> <br>\n" +
                "  <b class=\"total_sum\">Итоговая сумма: </b>\n" +
                "<br><br>\n" +
                "<p>Мы гарантируем качество и надежность наших товаров. Также, у нас есть специальные предложения и скидки для наших\n" +
                "    клиентов. Если у вас возникнут вопросы или потребуется дополнительная информация, не стесняйтесь обращаться к нам.\n" +
                "    Мы всегда готовы помочь.</p>\n" +
                "<p>Спасибо за выбор нашей компании. Мы надеемся на плодотворное сотрудничество с вами!</p>\n" +
                "</body>\n" +
                "</html>");
    }

    private void clearPage() {
        replaceHtmlPage("<!doctype html>\n" +
                "<html lang=\"ru\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta name=\"viewport\"\n" +
                "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                "    <title>Document</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
    }

    private void replaceHtmlPage(String newContent) {
        try {
            Document doc = Jsoup.parse(new File("storage/offer/offer_for_client.html"), "UTF-8");
            doc.html(newContent);

            File outputFile = new File("storage/offer/offer_for_client.html");
            FileWriter fileWriter = new FileWriter(outputFile);
            fileWriter.write(doc.outerHtml());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public Page<Order> searchCustomerOrdersAndSort(Long customerId, Integer page, Integer size, Optional<String> productName,
                                                   Optional<String> description, Optional<String> stat) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return orderRepository.searchOrderByOrderedAndSortedThreeValues(productName, description, stat, customerId, pageable);
    }

    public Page<Order> searchOrdersAndSort(Integer page, Integer size, Optional<String> customerName, Optional<String> managerName, Optional<String> status, String email) {
        page = getPage(page);
        Pageable pageable = PageRequest.of(page, size);
        return orderRepository.searchOrderCustomersByOrdered(customerName, managerName, status, pageable, email);
    }

    public void changeLead(LeadChangeDTO lead) {
        var order = getOrderById(lead.getOrderId());
        if (lead.getEndDate() == null) {
            switch (lead.getLead()) {
                case "Холодный":
                    order.setLead(lead.getLead());
                    order.setLeadStart(LocalDateTime.now());
                    order.setLeadEnd(LocalDateTime.now().plusDays(45));
                    break;
                case "Теплый":
                    order.setLead(lead.getLead());
                    order.setLeadStart(LocalDateTime.now());
                    order.setLeadEnd(LocalDateTime.now().plusDays(30));
                    break;
                case "Горячий":
                    order.setLead(lead.getLead());
                    order.setLeadStart(LocalDateTime.now());
                    order.setLeadEnd(LocalDateTime.now().plusDays(15));
                    break;
            }
        } else {
            order.setLead(lead.getLead());
            order.setLeadStart(LocalDateTime.now());
            order.setLeadEnd(lead.getEndDate());
        }

        orderRepository.save(order);
    }

    public void cancelOrder(Long id) {
        Order order = getOrderById(id);
        Request request = order.getRequest();
        Status status = statusRepository.findById(1L).orElseThrow();

        request.setStatus(status);
        orderRepository.delete(order);
    }

    public List<Long> findOrdersWithExpiredLeads(String email) {

        return orderRepository.findOrdersWithAnExpiredLead(email,LocalDateTime.now());
    }

    public void deleteLead(Long orderId) {
        var order = getOrderById(orderId);
        order.setLead(null);
        order.setLeadStart(null);
        order.setLeadEnd(null);
        orderRepository.save(order);
    }

    public Page<Order> searchOrdersAndSort(int page, int size,
                                           Optional<String> customerName, Optional<String> managerName, Optional<String> status,
                                           String sortBy, int sortOrder, String email) {
        page = getPage(page);
        Pageable pageable;

        if (sortOrder == 1) {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sortBy));
        } else {
            pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, sortBy));
        }
        return orderRepository.searchOrderCustomersByOrdered(customerName, managerName, status, pageable, email);

    }

    public Order getManagerOrder(Long orderId, Long managerId) {
        if(!orderRepository.existsOrderByIdAndEmployeeId(orderId,managerId)){
            throw new IllegalArgumentException("Заказ не принадлежит вам");
        }
        return orderRepository.findOrderById(orderId).orElseThrow(()-> new IllegalArgumentException("Такого заказа нету"));
    }
}

