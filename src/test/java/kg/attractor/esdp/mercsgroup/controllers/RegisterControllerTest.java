package kg.attractor.esdp.mercsgroup.controllers;

import kg.attractor.esdp.mercsgroup.dtos.authorization.RegistrationDTO;
import kg.attractor.esdp.mercsgroup.entities.Csrf;
import kg.attractor.esdp.mercsgroup.services.CsrfService;
import kg.attractor.esdp.mercsgroup.services.RegistrationService;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RegisterControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @MockBean
    private RegistrationService registrationService;

    @MockBean
    private CsrfService csrfService;

    @Test
    public void testGenerateCsrfLink() throws Exception{
        String csrfToken = "csrfToken";
        Csrf csrf = TestUtil.createCsrfObject(csrfToken);
        when(csrfService.generateCsrfLink(csrfToken)).thenReturn(csrf);
        mockMvc.perform(get("/register/generate-csrf-link")
                .param("c", csrfToken)
                .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    public void testRegisterPage_CsrfIsEmpty() throws Exception{
        String csrfToken = " ";
        when(csrfService.getCsrfByToken(csrfToken)).thenReturn(Optional.empty());
        mockMvc.perform(get("/register")
                .param("c", csrfToken))
                .andExpect(status().isOk())
                .andExpect(view().name("error/registerFail"));
    }

    @Test
    public void testRegisterPage_ValidData() throws Exception{
        String csrfToken = "csrfToken";
        when(csrfService.getCsrfByToken(csrfToken)).thenReturn(Optional.of(TestUtil.createCsrfObject(csrfToken)));
        when(csrfService.validateCsrf(csrfToken)).thenReturn(true);
        mockMvc.perform(get("/register")
                        .param("c", csrfToken))
                .andExpect(status().isOk())
                .andExpect(view().name("register"));
    }

    @Test
    public void testRegisterPage_NotValidData() throws Exception{
        String csrfToken = "csrfToken";
        when(csrfService.getCsrfByToken(csrfToken)).thenReturn(Optional.of(TestUtil.createCsrfObject(csrfToken)));
        when(csrfService.validateCsrf(csrfToken)).thenReturn(false);
        mockMvc.perform(get("/register")
                        .param("c", csrfToken))
                .andExpect(status().isOk())
                .andExpect(view().name("error/registerFail"));
    }

    @Test
    public void testRegistrationProcess_ValidData() throws Exception{
        RegistrationDTO registrationDTO = TestUtil.createRegistrationDto();
        when(registrationService.registerNewCustomer(any(), any())).thenReturn(true);
        mockMvc.perform(post("/register")
                .param("email", registrationDTO.getEmail())
                .param("password", registrationDTO.getPassword())
                .param("phone", registrationDTO.getPhone())
                .param("company", registrationDTO.getCompany())
                .param("csrfToken", registrationDTO.getCsrfToken())
                .param("name", registrationDTO.getName())
                .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/login"));
        verify(csrfService, never()).getCsrfByToken(any());
        verify(registrationService, times(1)).registerNewCustomer(any(), any());
        verify(csrfService, times(1)).deleteCsrf(registrationDTO.getCsrfToken());
    }

    @Test
    public void testRegistrationProcess_NotValidData() throws Exception{
        when(csrfService.getCsrfByToken("")).thenReturn(Optional.of(TestUtil.createCsrfObject("")));
        mockMvc.perform(post("/register")
                        .param("email", "")
                        .param("password", "")
                        .param("phone", "")
                        .param("company", "")
                        .param("csrfToken", "")
                        .param("name", "")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("register"));
        verify(csrfService, times(1)).getCsrfByToken("");
        verify(registrationService, never()).registerNewCustomer(any(), any());
        verify(csrfService, never()).deleteCsrf(any());
    }

    @Test
    public void testRegistrationProcess_UnsuccessfulRegistration() throws Exception{
        RegistrationDTO registrationDTO =  TestUtil.createRegistrationDto();
        when(registrationService.registerNewCustomer(any(), any())).thenReturn(false);
        mockMvc.perform(post("/register")
                        .param("email", registrationDTO.getEmail())
                        .param("password", registrationDTO.getPassword())
                        .param("phone", registrationDTO.getPhone())
                        .param("company", registrationDTO.getCompany())
                        .param("csrfToken", registrationDTO.getCsrfToken())
                        .param("name", registrationDTO.getName())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(view().name("register"));
        verify(csrfService, never()).getCsrfByToken(any());
        verify(registrationService, times(1)).registerNewCustomer(any(), any());
        verify(csrfService, never()).deleteCsrf(registrationDTO.getCsrfToken());
    }
}