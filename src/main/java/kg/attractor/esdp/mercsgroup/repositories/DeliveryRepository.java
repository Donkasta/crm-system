package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Delivery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryRepository extends JpaRepository<Delivery,Long> {
}
