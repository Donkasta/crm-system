package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.detail.DetailDTO;
import kg.attractor.esdp.mercsgroup.dtos.order.OrderEditDTO;
import kg.attractor.esdp.mercsgroup.entities.*;
import kg.attractor.esdp.mercsgroup.repositories.DetailRepository;
import kg.attractor.esdp.mercsgroup.repositories.OrderRepository;
import kg.attractor.esdp.mercsgroup.repositories.RequestRepository;
import kg.attractor.esdp.mercsgroup.repositories.StatusRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
class OrderServiceTest {

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private StatusRepository statusRepository;

    @MockBean
    private DetailRepository detailRepository;

    @MockBean
    private RequestRepository requestRepository;

    @Autowired
    private OrderService orderService;

    private static final String EMAIL = "manager1@mail.ru";

    @Test
    public void testGetOrdersByEmployee_ReturnsPageOfOrders() {
        String email = EMAIL;
        int page = 0;
        int size = 5;
        String sortBy = "customer";
        int sortOrder = 1;
        Sort sort = Sort.by(sortBy).ascending();
        Pageable pageable = PageRequest.of(page, size,sort);
        Page<Order> ordersPage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(orderRepository.findAllByEmployeesEmail(email, pageable)).thenReturn(ordersPage);

        Page<Order> result = orderService.getOrdersByEmployee(email, page, size, sortBy, sortOrder);

        verify(orderRepository).findAllByEmployeesEmail(email, pageable);
        assertEquals(ordersPage, result);
    }

    @Test
    public void testGetPage_PositivePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        int inputPage = 3;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, OrderService.class, orderService);
        assertEquals(inputPage - 1, result);
    }

    @Test
    public void testGetPage_NegativePageNumber_ReturnsCorrectPageNumber() throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        int inputPage = -50;
        int result = TestUtil.getDeclaredGetPageMethod(inputPage, OrderService.class, orderService);
        assertEquals(0, result);
    }

    @Test
    public void testEditOrder_ValidOrder_ReturnsTrue(){
        DetailDTO detailDTO = DetailDTO.from(TestUtil.createDetail());
        Order order = TestUtil.createOrder(EMAIL);
        OrderEditDTO orderEditDTO = OrderEditDTO.builder()
                .id(order.getId())
                .details(order.getDetails().stream().map(DetailDTO::from).collect(Collectors.toList()))
                .build();

        Status status = TestUtil.createStatus();
        status.setStatus("В обработке");

        when(orderRepository.findOrderById(orderEditDTO.getId())).thenReturn(Optional.of(order));
        when(detailRepository.findById(detailDTO.getId())).thenReturn(Optional.of(TestUtil.createDetail()));
        when(statusRepository.findStatusByStatus(status.getStatus())).thenReturn(Optional.of(TestUtil.createStatus()));

        Boolean bool = orderService.editOrder(orderEditDTO);
        verify(orderRepository, times(1)).findOrderById(orderEditDTO.getId());
        verify(detailRepository, times(1)).findById(detailDTO.getId());
        verify(detailRepository, times(1)).save(TestUtil.createDetail());
        verify(orderRepository, times(1)).save(order);
        assertEquals(bool, true);
    }

    @Test
    public void testEditOrder_NotValidOrder_ReturnsFalse(){
        DetailDTO detailDTO = DetailDTO.from(TestUtil.createDetail());
        Order order = TestUtil.createOrder(EMAIL);
        Detail detail = order.getDetails().get(0);
        detail.setQuantity(0.0);
        OrderEditDTO orderEditDTO = OrderEditDTO.builder()
                .id(order.getId())
                .details(List.of(DetailDTO.from(detail)))
                .build();
        Status status = TestUtil.createStatus();
        status.setStatus("В обработке");

        when(orderRepository.findOrderById(orderEditDTO.getId())).thenReturn(Optional.of(order));
        when(detailRepository.findById(detailDTO.getId())).thenReturn(Optional.ofNullable(TestUtil.createDetail()));
        when(statusRepository.findStatusByStatus(status.getStatus())).thenReturn(Optional.of(TestUtil.createStatus()));

        Boolean bool = orderService.editOrder(orderEditDTO);
        verify(orderRepository, times(1)).findOrderById(orderEditDTO.getId());
        verify(detailRepository, times(1)).findById(detailDTO.getId());
        verify(detailRepository, never()).save(any());
        verify(orderRepository, never()).save(any());
        assertEquals(bool, false);
    }

    @Test
    public void testGetOrderByIdOrThrowException(){
        Long id = 1L;
        Order initOrder = TestUtil.createOrder(EMAIL);
        when(orderRepository.findOrderById(id)).thenReturn(Optional.of(initOrder));
        Order order = orderService.getOrderById(id);
        assertEquals(order, initOrder);
    }

    @Test
    public void testGetOrderByIdOrThrowException_OrderNotFound() {
        Long orderId = 1L;

        when(orderRepository.findOrderById(orderId)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> orderService.getOrderById(orderId));

        verify(orderRepository, times(1)).findOrderById(orderId);
    }

    @Test
    public void testConfirmOrder_OrderStatusWaitingForConfirmation() {
        Long id = 1L;

        String confirmationText = "Ожидается подтверждение";
        String waitingText = "Ожидается оплата";
        String completedText = "Завершен";
        Order order = TestUtil.createOrder(EMAIL);
        Status statusWaitingForConfirmation = Status.builder()
                .status(confirmationText)
                .build();

        Status statusWaitingForPayment = Status.builder()
                .status(waitingText)
                .build();

        Status statusCompleted = Status.builder()
                .status(completedText)
                .build();

        Request request = TestUtil.createRequest(EMAIL);

        when(statusRepository.findStatusByStatus(confirmationText))
                .thenReturn(Optional.of(statusWaitingForConfirmation));
        when(statusRepository.findStatusByStatus(waitingText))
                .thenReturn(Optional.of(statusWaitingForPayment));
        when(statusRepository.findStatusByStatus(completedText))
                .thenReturn(Optional.of(statusCompleted));
        when(requestRepository.findById(id)).thenReturn(Optional.of(request));

        orderService.confirmOrder(order, statusWaitingForConfirmation.getStatus());

        verify(requestRepository, times(1)).save(request);
        verify(orderRepository, times(1)).save(order);
    }
}