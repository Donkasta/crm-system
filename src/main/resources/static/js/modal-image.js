function openImageModal(file) {
    const modal = document.getElementById(file);
    modal.style.display = "block";
}

function closeImageModal(file) {
    const modal = document.getElementById(file);
    modal.style.display = "none";
}

function openRequestImageModal(imgElement) {
    const modal = document.getElementById("request-img-modal");
    const modalImg = document.getElementById("requestModalImage");

    modal.style.display = "block";
    modalImg.src = imgElement.src;
}

function closeRequestImageModal() {
    const modal = document.getElementById("request-img-modal");
    modal.style.display = "none";
}


