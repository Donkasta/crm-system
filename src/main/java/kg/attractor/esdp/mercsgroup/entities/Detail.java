package kg.attractor.esdp.mercsgroup.entities;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "details")
@Builder(toBuilder = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Detail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;
    @Column(name = "product_url")
    private String productUrl;
    private Double quantity;
    @Column(name = "quantity_unit")
    private String quantityUnit;
    @Column(name = "product_price")
    private Double productPrice;
    private Double freight;
    private float percentage;
    @Column(name = "final_cost")
    private Double finalCost;
    @Column(name = "customs_value")
    private Double customsValue;

}
