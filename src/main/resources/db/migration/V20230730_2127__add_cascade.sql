ALTER TABLE customers
    DROP CONSTRAINT IF EXISTS customers_activity_id_fkey,
    DROP CONSTRAINT IF EXISTS customers_employee_id_fkey,
    DROP CONSTRAINT IF EXISTS customers_role_id_fkey,
    ADD CONSTRAINT customers_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE,
    add constraint customers_activity_id_fkey foreign key (activity_id)
        references activities (id) on delete cascade on update cascade,
    add constraint customers_employee_id_fkey foreign key (employee_id)
        references employees (id) on delete cascade on update cascade;

ALTER TABLE employees
    DROP CONSTRAINT IF EXISTS employees_role_id_fkey,
    ADD CONSTRAINT employees_role_id_fkey FOREIGN KEY (role_id)
        REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE requests
    DROP CONSTRAINT IF EXISTS requests_delivery_id_fkey,
    DROP CONSTRAINT IF EXISTS requests_customer_id_fkey,
    DROP CONSTRAINT IF EXISTS requests_status_id_fkey,
    ADD CONSTRAINT requests_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customers (id) ON DELETE CASCADE,
    ADD CONSTRAINT requests_delivery_id_fkey FOREIGN KEY (delivery_id)
        REFERENCES delivery_types (id) ON DELETE CASCADE,
    ADD CONSTRAINT requests_status_id_fkey FOREIGN KEY (status_id)
        REFERENCES statuses (id) ON DELETE CASCADE;

ALTER TABLE files
    DROP CONSTRAINT IF EXISTS files_request_id_fkey,
    ADD CONSTRAINT files_request_id_fkey FOREIGN KEY (request_id)
        REFERENCES requests (id) ON DELETE CASCADE;

ALTER TABLE orders
    DROP CONSTRAINT IF EXISTS orders_customer_id_fkey,
    DROP CONSTRAINT IF EXISTS orders_employee_id_fkey,
    DROP CONSTRAINT IF EXISTS orders_request_id_fkey,
    DROP CONSTRAINT IF EXISTS orders_status_id_fkey,
    ADD CONSTRAINT orders_customer_id_fkey FOREIGN KEY (customer_id)
        REFERENCES customers (id) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT orders_employee_id_fkey FOREIGN KEY (employee_id)
        REFERENCES employees (id) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT orders_request_id_fkey FOREIGN KEY (request_id)
        REFERENCES requests (id) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT orders_status_id_fkey FOREIGN KEY (status_id)
        REFERENCES statuses (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE details
    DROP CONSTRAINT IF EXISTS details_order_id_fkey,
    ADD CONSTRAINT details_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE;