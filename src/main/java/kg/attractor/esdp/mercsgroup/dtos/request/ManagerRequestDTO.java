package kg.attractor.esdp.mercsgroup.dtos.request;

import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@Validated
public class ManagerRequestDTO {
    @NotBlank(message = "Введите название")
    private String product;
    @NotBlank(message = "Введите описание")
    private String description;
    @NotBlank(message = "Введите количество")
    private String quantity;
    private String information;

    @Size(max = 10, message = "Количество файлов должно быть до 10")
    private MultipartFile[] files;
    @NotBlank(message = "Введите имя клиента")
    private String customerName;
    @NotBlank(message = "Введите номер клиента")
    @Pattern(regexp = "^(\\+996\\s?)?(\\d{3}\\s?)?\\d{6}$", message = "Введите номер телефона в формате: +996000000000")
    private String customerNumber;
    @Email(message = "Введите email правильно")
    private String customerEmail;
    private String customerCompany;
    private Long deliveryId;
}
