package kg.attractor.esdp.mercsgroup.repositories;

import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.entities.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {
    @Query(value = "SELECT * FROM tokens t WHERE t.user_id = :userId AND t.role_id = :roleId", nativeQuery = true)
    Optional<Token> findByUserIdAndRoleId(@Param("userId") Long userId, @Param("roleId") Long roleId);


    @Modifying
    @Transactional
    @Query(value = "UPDATE Token t SET t.token = :token WHERE t.user_id = :userId AND t.role = :role")
    void setTokenByUserIdAndRole(@Param("token") String token, @Param("userId") Long userId,
                                 @Param("role") Role role);
}
