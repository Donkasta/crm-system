INSERT INTO roles (role) VALUES ('Manager');
INSERT INTO roles (role) VALUES ('Customer');
INSERT INTO roles (role) VALUES ('Admin');


INSERT INTO employees (email, password, full_name, inn, enabled, login, number, post, role_id) VALUES ('manager1@mail.ru', '$2a$10$MJPJ0gT1q.TUXBMB/hOyX.nXAqOgnf1GJmgQKfoZSIatrXJPZhSV2', 'Manager1 manager1', '22222222222222', true, 'manager1', '+996555555555', 'Manager', 1);
INSERT INTO employees (email, password, full_name, inn, enabled, login, number, post, role_id) VALUES ('manager2@mail.ru', '$2a$10$iGxtcyLGrg/gsla9B/.xJuEDTSjL66cIraKAPfGv0mI8c0aG7fqXW', 'Manager2 manager2', '33333333333333', true, 'manager2', '+996666666666', 'Manager', 1);
INSERT INTO employees (email, password, full_name, inn, enabled, login, number, post, role_id) VALUES ('admin@mail.ru', '$2a$10$n09gslPkqcNDDlrNOrqsM.5MVN2uSAMxhkg6EmvnObPhvNiCpRHlK', 'Admin admin', '44444444444444', true, 'admin', '+996777777777', 'Administrator', 3);
