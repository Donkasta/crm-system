package kg.attractor.esdp.mercsgroup.dtos.util;

import kg.attractor.esdp.mercsgroup.entities.Delivery;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeliveryDTO {

    public static DeliveryDTO from(Delivery delivery){
        return builder()
                .id(delivery.getId())
                .delivery(delivery.getDelivery())
                .build();
    }

    private Long id;

    private String delivery;
}
