package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.dtos.authorization.PasswordRecoveryDTO;
import kg.attractor.esdp.mercsgroup.entities.Customer;
import kg.attractor.esdp.mercsgroup.entities.Employee;
import kg.attractor.esdp.mercsgroup.entities.Role;
import kg.attractor.esdp.mercsgroup.entities.Token;
import kg.attractor.esdp.mercsgroup.repositories.CustomerRepository;
import kg.attractor.esdp.mercsgroup.repositories.EmployeeRepository;
import kg.attractor.esdp.mercsgroup.repositories.TokenRepository;
import kg.attractor.esdp.mercsgroup.utils.EmailUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class PasswordRecoveryService {
    private final CustomerRepository customerRepository;
    private final EmployeeRepository employeeRepository;
    private final TokenRepository tokenRepository;
    public static final Integer PASSWORD_RECOVERY_TOKEN_LENGTH = 4;

    public boolean sendPasswordRecoveryTokenToEmail(String email){
        Optional<Customer> optionalCustomer = customerRepository.findByEmail(email);
        Optional<Employee> optionalEmployee = employeeRepository.findByEmail(email);
        if (optionalCustomer.isPresent()) {
            Customer customer = optionalCustomer.get();
            sendingProcess(customer.getId(), customer.getRole(), email);
            return true;
        } else if (optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();
            sendingProcess(employee.getId(), employee.getRole(), email);
            return true;
        }
        return false;
    }

    private void sendingProcess(Long userId, Role role, String email){
        Optional<Token> optionalToken = tokenRepository.findByUserIdAndRoleId(userId, role.getId());

        String token = generateRandomNumberString();
        EmailUtil.sendPasswordResetEmail(email, token);

        if (optionalToken.isPresent()) {
            Token existingToken = optionalToken.get();
            existingToken.setToken(token);
            tokenRepository.setTokenByUserIdAndRole(token, userId, role);
        } else {
            Token newToken = Token.builder()
                    .user_id(userId)
                    .role(role)
                    .token(token)
                    .build();
            tokenRepository.save(newToken);
        }
    }


    public boolean confirmToken(String email, String token){
        Optional<Customer> optionalCustomer = customerRepository.findByEmail(email);
        Optional<Employee> optionalEmployee = employeeRepository.findByEmail(email);
        if(optionalCustomer.isPresent()) {
            Optional<Token> optionalToken = tokenRepository.findByUserIdAndRoleId(optionalCustomer.get().getId(),
                    optionalCustomer.get().getRole().getId());
            return optionalToken.isPresent() && optionalToken.get().getToken().equals(token);
        } else if (optionalEmployee.isPresent()){
            Optional<Token> optionalToken = tokenRepository.findByUserIdAndRoleId(optionalEmployee.get().getId(),
                    optionalEmployee.get().getRole().getId());
            return optionalToken.isPresent() && optionalToken.get().getToken().equals(token);
        }
        return false;
    }


    public void recoverPassword(PasswordRecoveryDTO passwordRecoveryDTO){
        Optional<Customer> optionalCustomer = customerRepository.findByEmail(passwordRecoveryDTO.getEmail());
        Optional<Employee> optionalEmployee = employeeRepository.findByEmail(passwordRecoveryDTO.getEmail());
        if(optionalCustomer.isPresent()) {
            Optional<Token> optionalToken = tokenRepository.findByUserIdAndRoleId(optionalCustomer.get().getId(),
                    optionalCustomer.get().getRole().getId());
            if (optionalToken.isPresent()) {
                tokenRepository.delete(optionalToken.get());
                customerRepository.setPasswordById(hashPassword(passwordRecoveryDTO.getPassword()),
                        optionalCustomer.get().getId());
            }
        } else if (optionalEmployee.isPresent()){
            Optional<Token> optionalToken = tokenRepository.findByUserIdAndRoleId(optionalEmployee.get().getId(),
                    optionalEmployee.get().getRole().getId());
            if (optionalToken.isPresent()) {
                tokenRepository.delete(optionalToken.get());
                employeeRepository.setPasswordById(hashPassword(passwordRecoveryDTO.getPassword()),
                        optionalEmployee.get().getId());
            }
        }
    }

    private String generateRandomNumberString() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder(PasswordRecoveryService.PASSWORD_RECOVERY_TOKEN_LENGTH);
        for (int i = 0; i < PasswordRecoveryService.PASSWORD_RECOVERY_TOKEN_LENGTH; i++) {
            int randomNumber = random.nextInt(10);
            sb.append(randomNumber);
        }
        return sb.toString();
    }

    private String hashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
