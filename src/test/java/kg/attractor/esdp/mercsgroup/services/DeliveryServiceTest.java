package kg.attractor.esdp.mercsgroup.services;

import kg.attractor.esdp.mercsgroup.entities.Delivery;
import kg.attractor.esdp.mercsgroup.repositories.DeliveryRepository;
import kg.attractor.esdp.mercsgroup.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class DeliveryServiceTest {

    @MockBean
    private DeliveryRepository deliveryRepository;

    @Autowired
    private DeliveryService deliveryService;

    @Test
    public void testGetAll_DeliveriesFound(){
        List<Delivery> deliveryList = List.of(TestUtil.createDelivery());
        when(deliveryRepository.findAll()).thenReturn(deliveryList);

        List<Delivery> foundDeliveryList = deliveryService.getAll();
        assertEquals(deliveryList, foundDeliveryList);
        verify(deliveryRepository, times(1)).findAll();
    }
}